package com.example.demafayz.takedaconferencesuat;

import android.app.Application;
import android.content.Context;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.*;
import de.greenrobot.event.EventBus;

import java.io.IOException;
import java.net.Proxy;

import static com.example.demafayz.takedaconferencesuat.util.Constants.*;

public class App extends Application {

    private static App sInstance;

    private static OkHttpClient sClient;


    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        initRestAdapter();
    }


    /**
     * creates and accepts custom Gson and request interceptors for Retrofit adapter
     */
    private void initRestAdapter() {
        sClient = new OkHttpClient();
        sClient.setAuthenticator(new Authenticator() {
            @Override
            public Request authenticate(Proxy proxy, Response response) throws IOException {
                String credential = Credentials.basic(CLIENT_ID, CLIENT_SECRET);
                return response.request().newBuilder().header("Authorization", credential).build();
            }

            @Override
            public Request authenticateProxy(Proxy proxy, Response response) throws IOException {
                return null;
            }
        });
        /*sRestAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(sClient))
                .setConverter(new GsonConverter(gson))
                .build();*/
    }


    /**
     * returns link to application context
     */
    public static App get() {
        return sInstance;
    }


    public static OkHttpClient getHttpClient() {
        return sClient;
    }

    private static EventBus eventBus;
    public static EventBus bus() {
        if (eventBus == null) {
            eventBus = EventBus.getDefault();
        }
        return eventBus;
    }

    public static String getAccessToken() {
        return get().getSharedPreferences(Constants.APPLICATION, Context.MODE_PRIVATE)
                    .getString(Constants.API_TOKEN_KEY, "");
    }

    private static Gson gson;
    public static Gson gson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES) // provides camel case recognization
                    .create();
        }
        return gson;
    }
}
