package com.example.demafayz.takedaconferencesuat.screens.conference;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.dataExamples.Conference;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.List;

/**
 * Created by demafayz on 26.08.15.
 */
public class ConferenceAdapter extends ArrayAdapter<Conference> {

    private class ViewHolder {
        ImageView ivConferenceIcon;
        TextView tvConferenceDate;
        TextView tvConferenceAddress;
        TextView tvTitle;
        TextView tvDesc;
    }

    private ViewHolder vh = new ViewHolder();
    private List<Conference> list;
    private LayoutInflater inflater;
    private static final String TAG = "MyTag ConferenceAdapter";


    public ConferenceAdapter(Context context, int resource, List<Conference> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = inflater.inflate(R.layout.conference_list_item, parent, false);
        vh.ivConferenceIcon = (ImageView) itemView.findViewById(R.id.ivConferenceIcon);
        vh.tvConferenceDate = (TextView) itemView.findViewById(R.id.tvConferenceDate);
        vh.tvConferenceAddress = (TextView) itemView.findViewById(R.id.tvConferenceAddress);
        vh.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        vh.tvDesc = (TextView) itemView.findViewById(R.id.item_conf_descr);
        Log.d(TAG, "list.get(position).getIcon():" + list.get(position).getLogo());
        UrlImageViewHelper.setUrlDrawable(vh.ivConferenceIcon, list.get(position).getLogo(), R.drawable.alpha_pix, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        vh.tvConferenceAddress.setText(list.get(position).getCity());
        vh.tvConferenceDate.setText(ApiHelper.longDateToString(list.get(position).getStartDate(), "MMMM yyyy"));
        vh.tvTitle.setText(list.get(position).getTitle());
        vh.tvDesc.setText(list.get(position).getAddress());
        return itemView;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.conference_list_item, parent, false);
            vh.ivConferenceIcon = (ImageView) itemView.findViewById(R.id.ivConferenceIcon);
            vh.tvConferenceDate = (TextView) itemView.findViewById(R.id.tvConferenceDate);
            vh.tvConferenceAddress = (TextView) itemView.findViewById(R.id.tvConferenceAddress);
            vh.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            itemView.setTag(vh);
        } else {
            vh = (ViewHolder) itemView.getTag();
        }
        UrlImageViewHelper.setUrlDrawable(vh.ivConferenceIcon, list.get(position).getIcon(), R.mipmap.ic_launcher, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        vh.tvConferenceAddress.setText(list.get(position).getAddress());
        vh.tvConferenceDate.setText(list.get(position).getDate_start());
        vh.tvTitle.setText(list.get(position).getTitle());
        itemView.setTag(vh);
        return itemView;
    }*/
}
