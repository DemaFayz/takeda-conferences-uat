package com.example.demafayz.takedaconferencesuat.util;

import com.example.demafayz.takedaconferencesuat.dataExamples.MapEntityPoint;

import java.util.List;

public class MapActivityExtraJson {
    private final String map_url;
    private final List<MapEntityPoint> events;

    public String getMap_url() {
        return map_url;
    }

    public List<MapEntityPoint> getEvents() {
        return events;
    }

    public MapActivityExtraJson(String map_url, List<MapEntityPoint> events) {

        this.map_url = map_url;
        this.events = events;
    }
}
