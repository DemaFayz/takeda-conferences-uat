package com.example.demafayz.takedaconferencesuat.api;

/**
 * Created by demafayz on 09.09.15.
 */
public class UserToken {
    private final long id;
    private final String password;
    private final String last_login;
    private final String name;
    private final String email;
    private final boolean is_active;
    private final boolean is_admin;
    private final int profession;


    public UserToken(long id, String password, String last_login, String name, String email, boolean is_active, boolean is_admin, int profession) {
        this.id = id;
        this.password = password;
        this.last_login = last_login;
        this.name = name;
        this.email = email;
        this.is_active = is_active;
        this.is_admin = is_admin;
        this.profession = profession;
    }

    public long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getLast_login() {
        return last_login;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public boolean is_active() {
        return is_active;
    }

    public boolean is_admin() {
        return is_admin;
    }

    public int getProfession() {
        return profession;
    }
}
