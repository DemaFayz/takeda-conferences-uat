package com.example.demafayz.takedaconferencesuat.screens.conference.map;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.dataExamples.MapEntityPoint;
import com.example.demafayz.takedaconferencesuat.dialog.ProgressDialog;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.example.demafayz.takedaconferencesuat.util.MapActivityExtraJson;
import com.example.demafayz.takedaconferencesuat.util.view.ImageLayout;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import static android.view.View.*;

public class MapActivity extends ActionBarActivity {

    private Toolbar toolbar;
    private ActionBar actionBar;

    private class ViewHolder {
        ImageLayout ivMap;
    }

    private ViewHolder vh = new ViewHolder();
    private List<MapEntityPoint> pins;
    private String mapImageUrl;
    private static String TAG = "MyTag MapActivity";
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        setContentView(R.layout.activity_map); //TODO create ImageLayout programmatically so size is not constant
        populateActionBar();
        populateViewHolder();
        loadDataFromIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setLocations();
    }

    private void setLocations() {
        (new AsyncTask<String, Void, Void> (){
            Bitmap bitmap;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(String... params) {
                try {
                    bitmap = Picasso.with(MapActivity.this)
                            .load(mapImageUrl)
                            .get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                ImageLayout il = (ImageLayout) findViewById(R.id.rlMapParent);
                il.setImageResource(bitmap);
                il.setVisibility(VISIBLE);

                for (MapEntityPoint pin : pins) {
                    putLocation(pin.getX(), pin.getY(), pin.getDescription());
                }
                progressDialog.dismiss();
            }
        }).execute();
    }

    private void putLocation(int x, int y, String title) {
        LinearLayout pinContainer = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.pin, null);
        pinContainer.setVisibility(VISIBLE);
        ImageLayout.LayoutParams containerLP = new ImageLayout.LayoutParams();
        containerLP.centerX = x;
        containerLP.centerY = y;

        TextView pinTv = (TextView) pinContainer.findViewById(R.id.pin_text);
        pinTv.setText(title);

        vh.ivMap.addView(pinContainer, containerLP);
    }



    private void loadDataFromIntent() {
        MapActivityExtraJson extra = App.gson().fromJson(getIntent().getStringExtra(Constants.MAP_EXTRA_KEY), MapActivityExtraJson.class);
        pins = extra.getEvents();
        mapImageUrl = extra.getMap_url();
    }

    private void populateViewHolder() {
        vh.ivMap = (ImageLayout) findViewById(R.id.rlMapParent);
    }

    private void populateActionBar() {

        toolbar = (Toolbar) findViewById(R.id.appBar);
        toolbar.setTitle("Карта");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}