package com.example.demafayz.takedaconferencesuat.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.login.logo.LogoFragment;
import com.example.demafayz.takedaconferencesuat.adapters.NavigationDrawerAdapter;
import com.example.demafayz.takedaconferencesuat.data.NavigationDrawerItem;
import com.example.demafayz.takedaconferencesuat.screens.conference.ConferenceFragment;
import com.example.demafayz.takedaconferencesuat.takeda.TakedaFragment;
import com.example.demafayz.takedaconferencesuat.warning.WarningFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {


    public NavigationDrawerFragment() {

    }

    private class ViewHolder {
        ListView lvNavigation;
    }

    private ViewHolder vh = new ViewHolder();
    private List<NavigationDrawerItem> list = new ArrayList<>();
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private static final String PREF_FILE_NAME = "navigpref";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private boolean mUserLernedDrawer;
    private boolean mFromSavedInstanceState;

    /*Fragments*/
    private FragmentTransaction ft;
    private static FragmentManager fm;
    public ConferenceFragment conferenceFragment = new ConferenceFragment();
    private LecturerFragment lecturerFragment = new LecturerFragment();
    private WarningFragment warningFragment = new WarningFragment();
    private TakedaFragment takedaFragment = new TakedaFragment();
    private AgreementFragment agreementFragment = new AgreementFragment();
    private LogoFragment logoFragment = new LogoFragment();
    //private DrugsFragment drugsFragment = new DrugsFragment();
    private static int position = 0;
    private static final String TAG = "MyTag NavigationDrawerFragment";
    private MedicineFragment medicineFragment = new MedicineFragment();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        populateViewHolder(layout);
        populateList();
        createNewAdapter(position);
        navigationClickedRegister();
        showFragment(0, true);
        return layout;
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(containerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    private void navigationClickedRegister() {
        vh.lvNavigation.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showFragment(position, false);
            }
        });
    }

    public void showFragment(int position, boolean first) {
        fm = getActivity().getSupportFragmentManager();
        ft = fm.beginTransaction();
        switch (position) {
            case 0: {
                ft.replace(R.id.fragmentContainer, conferenceFragment);
            }
            break;
            case 1: {
                ft.replace(R.id.fragmentContainer, takedaFragment);

            }
            break;
            case 2: {
                ft.replace(R.id.fragmentContainer, lecturerFragment);
            }
            break;
            case 3: {
                ft.replace(R.id.fragmentContainer, medicineFragment);
            }
            break;
            case 4: {
                ft.replace(R.id.fragmentContainer, warningFragment);
            }
            break;
            case 5: {
                ft.replace(R.id.fragmentContainer, agreementFragment);
            }break;
            case -1: {
                ft.replace(R.id.fragmentContainer, logoFragment);
            }break;
        }

        if (!first) {
            ft.addToBackStack(null);
            closeDrawer();
        }
        ft.commit();
        adapter.setSelectPosition(NavigationDrawerFragment.this.position = position);
        adapter.notifyDataSetChanged();
    }

    private void createNewAdapter(int position) {
        if (adapter != null) {
            adapter = null;
        }
        adapter = new NavigationDrawerAdapter(getActivity(), 0, list, position);
        vh.lvNavigation.setAdapter(adapter);
    }


    public void updateConferenceAdapter() {
        conferenceFragment.createNewAdapter();
    }


    private void populateList() {
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Конференции"));
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Takeda"));
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Лекторы"));
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Препараты"));
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Предупреждение"));
        list.add(new NavigationDrawerItem(R.drawable.navigation_icon_selected, R.drawable.navigation_icon_anselected, "Соглашение"));
    }

    private void populateViewHolder(View layout) {
        vh.lvNavigation = (ListView) layout.findViewById(R.id.lvNavigationDrawer);
    }
    
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigationDrawerOpen, R.string.navigationDrawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLernedDrawer) {
                    mUserLernedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLernedDrawer + "");
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };
        if (!mUserLernedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.closeDrawer(containerView);
        } else {
            mDrawerLayout.openDrawer(containerView);
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

}