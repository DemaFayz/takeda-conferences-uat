package com.example.demafayz.takedaconferencesuat.api.model;

public class AuthResponseJson {

    private final long expires_in;
    private final String refresh_token;
    private final String access_token;
    private final String scope;
    private final String token_type;

    public AuthResponseJson(long expires_in, String refresh_token, String access_token, String scope, String token_type) {
        this.expires_in = expires_in;
        this.refresh_token = refresh_token;
        this.access_token = access_token;
        this.scope = scope;
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getAccess_token() {
        return access_token;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public String getScope() {
        return scope;
    }

    public String getToken_type() {
        return token_type;
    }
}
