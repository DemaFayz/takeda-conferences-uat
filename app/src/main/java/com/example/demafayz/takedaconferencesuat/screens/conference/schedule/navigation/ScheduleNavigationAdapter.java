package com.example.demafayz.takedaconferencesuat.screens.conference.schedule.navigation;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;

import java.util.List;

/**
 * Created by demafayz on 08.09.15.
 */
public class ScheduleNavigationAdapter extends ArrayAdapter<String> {

    private List<String> list;
    private LayoutInflater inflater;
    private static final String TAG = "MyTag ScheduleNavigationAdapter";

    private class ViewHolder {
        TextView tvDate;
    }

    private ViewHolder vh = new ViewHolder();

    public ScheduleNavigationAdapter(Context context, int resource, List<String> list) {
        super(context, resource, list);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.item_schedule, parent, false);
            vh.tvDate = (TextView) itemView.findViewById(R.id.tvScheduleItem);
            itemView.setTag(vh);
        } else {
            vh = (ViewHolder) itemView.getTag();
        }
        vh.tvDate.setText(list.get(position));
        itemView.setTag(vh);
        return itemView;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
