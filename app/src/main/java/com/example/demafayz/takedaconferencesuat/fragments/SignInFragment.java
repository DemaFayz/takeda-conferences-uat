package com.example.demafayz.takedaconferencesuat.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment implements View.OnClickListener {

    private ViewHolder vh = new ViewHolder();
    private FragmentActivity activity;
    private SignUpFragment signUpFragment;
    private RecoveryPassFragment recoveryPassFragment;
    private FragmentManager fm;
    private FragmentTransaction ft;

    private class ViewHolder {
        public EditText etEmail;
        public EditText etPass;
        public TextView tvRecoveryPass;
        public Button btnSignIn;
        public Button btnSignUp;
    }

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_sign_in, container, false);
        populateDatas();
        populateViewHolder(layout);
        return layout;
    }

    private void populateViewHolder(View layout) {
        vh.btnSignIn = (Button) layout.findViewById(R.id.btn_sign_in);
        vh.btnSignIn.setOnClickListener(this);

        vh.btnSignUp = (Button) layout.findViewById(R.id.btn_sign_up);
        vh.btnSignUp.setOnClickListener(this);

        vh.etEmail = (EditText) layout.findViewById(R.id.et_email);
        vh.etPass = (EditText) layout.findViewById(R.id.et_pass);

        vh.tvRecoveryPass = (TextView) layout.findViewById(R.id.tv_recovery_pass);
        vh.tvRecoveryPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_sign_in:
                openMainApp();
                break;
            case R.id.btn_sign_up:
                openSignUp();
                break;
            case R.id.tv_recovery_pass:
                openRecoveryPass();
                break;
        }
    }

    private void openRecoveryPass() {
        recoveryPassFragment = RecoveryPassFragment.newInstance();
        ft.replace(R.id.login_fragment_container, recoveryPassFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void openSignUp() {
        signUpFragment = SignUpFragment.newInstance();
        ft.replace(R.id.login_fragment_container, signUpFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void openMainApp() {
        activity.finish();
        Intent mainAppIntent = new Intent(activity, MainActivity.class);
        startActivity(mainAppIntent);
    }

    private void populateDatas() {
        this.activity = getActivity();
        fm = activity.getSupportFragmentManager();
        ft = fm.beginTransaction();
    }

    public static SignInFragment newInstance() {

        Bundle args = new Bundle();

        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
