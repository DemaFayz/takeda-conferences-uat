package com.example.demafayz.takedaconferencesuat.activitys;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.api.model.AuthResponseJson;
import com.example.demafayz.takedaconferencesuat.dataBase.DBHelper;
import com.example.demafayz.takedaconferencesuat.dataBase.DataBase;
import com.example.demafayz.takedaconferencesuat.dataExamples.AppText;
import com.example.demafayz.takedaconferencesuat.fragments.NavigationDrawerFragment;

import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.google.gson.Gson;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import static com.example.demafayz.takedaconferencesuat.util.Constants.*;

public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MyTag MainActivity";
    private static Toolbar toolbar;
    private static boolean firstStartFlag = true;
    private NavigationDrawerFragment drawerFragment;
    private ProgressDialog progressDialog;
    public static JSONObject rootJson;
    private AlertDialog.Builder dialog;
    private static final String FIRST_START_KAY = "first_start_kay";
    private static final String PREF_FILE_NAME = "pref_file_name";

    private SharedPreferences sp = null;

    /*--- Data Base ---*/
    DataBase mDataBase;
    /*-----------------*/

    public static List<AppText> listAppText;


    public static void setTitle(String text) {
        toolbar.setTitle(text);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateActionBar();
        initDataBase();
        AppStart();
        populateNavigationDrawer();
        dialog = new AlertDialog.Builder(this);
        dialog.setTitle(null);
        dialog.setMessage(getResources().getString(R.string.not_net));
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        startAnimation();
        sp = getSharedPreferences(Constants.APPLICATION, MODE_PRIVATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sp.getString(LOGIN_KEY, null) != null) { //if auth already proceed
            loadRootJsonArray();
        }
    }

    private void loadTextPagesOnRootJson() {
        listAppText = ApiHelper.getTextPagesOnRootJson(MainActivity.rootJson);
    }

    private void loadRootJsonArray() {
        if (isNetworkAvailable()) {
            new GetRoot().execute();
        } else {
            AlertDialog.Builder dialogBilder = new AlertDialog.Builder(this);
            dialogBilder.setMessage(getResources().getString(R.string.not_net));
            dialogBilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialogBilder.create();
            dialogBilder.show();
        }
    }

    //Metod dlya proverki interneta
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void AppStart() {
        mDataBase.deleteTable(DBHelper.TABLE_CONFERENCE.TABLE_NAME);
        //new GetConf().execute();
        //Log.d(TAG, list.toString());

        /*List<ConferenceItem> list = new ArrayList<>();
        list.add(new ConferenceItem(1,
                "http://news.androidlista.fr/wp-content/uploads/sites/5/sites/5/2014/11/android.png",
                "Тестовая конфиренция 20151 август",
                "Х.Такташа д.43",
                "Житомир",
                "Украина",
                "тестовое описание конференции",
                "2 - августа 20151",
                "4 - августа 2015"));
        list.add(new ConferenceItem(1,
                "http://www.androidcentral.com/sites/androidcentral.com/files/postimages/684/podcast_featured_3.jpg",
                "Тестовая конфиренция 20151 август",
                "Х.Такташа д.43",
                "Житомир",
                "Украина",
                "тестовое описание конференции",
                "2 - августа 20151",
                "4 - августа 2015"));
        list.add(new ConferenceItem(1,
                "http://androidsmartfony.com/uploads/posts/2013-09/1378296429_android.jpg",
                "Тестовая конфиренция 20151 август",
                "Х.Такташа д.43",
                "Житомир",
                "Украина",
                "тестовое описание конференции",
                "2 - августа 20151",
                "4 - августа 2015"));*/
    }

    private void initDataBase() {
        mDataBase = new DataBase(this);
    }

    private void startAnimation() {
        /*if (Boolean.valueOf(readFromPreferences(this, FIRST_START_KAY, "true"))) {
            saveToPreferences(this, FIRST_START_KAY, "false");
            startActivity(new Intent(this, LoginActivity.class));
        }*/
        if (AppSupport.isNetworkAvailable(this)) {
            if (firstStartFlag) {
                firstStartFlag = false;
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        } else {
            dialog.show();
        }
    }

    private void populateNavigationDrawer() {
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fNavigationDrowerFragment);
        drawerFragment.setUp(R.id.fNavigationDrowerFragment, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
    }

    private void populateActionBar() {
        toolbar = (Toolbar) findViewById(R.id.appBar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    /*private class GetConf extends AsyncTask<Void, Void, Void> {
        private List<AppText> list = new ArrayList<>();
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Выполняется запрос");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            list = ApiHelper.loadAppTextDate();
            mDataBase.putAppText(list);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listAppText = list;
            dialog.cancel();
        }
    }*/

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Update Adapter", "0 onRestart Called");
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadRootJsonArray();
            }
        }).start();
    }

    private class GetRoot extends AsyncTask<Void, Void, Void> {
        JSONObject jsonObject;
        //ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //dialog = ProgressDialog.show(MainActivity.this, null, "Подождите...", false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String[] ps = new String[] { sp.getString(LOGIN_KEY, " "), sp.getString(PASSWORD_KEY, " ") };
            Response response = ApiHelper.processLogin(ps);
            AuthResponseJson responseJson = null;
            try {
                responseJson = new Gson()
                        .fromJson(response.body().string(), AuthResponseJson.class);
                if (response.isSuccessful()) {

                    String at = responseJson.getAccess_token();
                    App.get().getSharedPreferences(APPLICATION, Context.MODE_PRIVATE)
                            .edit()
                            .putString(API_TOKEN_KEY, at)
                            .commit();
                    jsonObject = ApiHelper.getRootJson();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            rootJson = jsonObject;
            loadTextPagesOnRootJson();
            drawerFragment.updateConferenceAdapter();
            //dialog.dismiss();
        }
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public  static String readFromPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, preferenceValue);
    }
}