package com.example.demafayz.takedaconferencesuat.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.demafayz.takedaconferencesuat.dataExamples.AppText;
import com.example.demafayz.takedaconferencesuat.dataExamples.Conference;
import com.example.demafayz.takedaconferencesuat.screens.conference.ConferenceItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by demafayz on 26.08.15.
 */
public class DataBase {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private Context context;
    private Cursor c;
    private static final String TAG = "MyTag DataBase";

    public DataBase(Context context) {
        this.context = context;
        //Log.d(TAG_DB, "CONSTRUCTOR");
        this.dbHelper = new DBHelper(context);
        this.db = this.dbHelper.getWritableDatabase();
        if (this.db == null) {
            Log.d(TAG, "dbHelper.getWritableDatabase() is NULL");
        } else {
            //    Log.d(TAG_DB, "WHERY GOOD!!!");
        }
    }



    public List<Conference> getConferenceList() {
        List<Conference> list = new ArrayList<>();
        c = db.query(DBHelper.TABLE_CONFERENCE.TABLE_NAME, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int pkColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.PK);
            int titleColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.TITLE);
            int startDateColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.START_DATE);
            int endDateColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.END_DATE);
            int cityColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.CITY);
            int addressColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.ADDRESS);
            int descColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.DESC);
            int logoColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.LOGO);
            int mapColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.MAP);
            int hallsColIndex = c.getColumnIndex(DBHelper.TABLE_CONFERENCE.HALLS);

            do {
                throw new IllegalStateException("Caching unsupported?");
//                list.add(new Conference(
//                        c.getLong(pkColIndex),
//                        c.getString(titleColIndex),
//                        c.getLong(startDateColIndex),
//                        c.getLong(endDateColIndex),
//                        c.getString(cityColIndex),
//                        c.getString(addressColIndex),
//                        c.getString(descColIndex),
//                        c.getString(logoColIndex),
//                        c.getString(mapColIndex),
//                        c.getString(hallsColIndex)));
            } while (c.moveToNext());
        } else {
            Log.d(TAG, "0_rows");
        } c.close();
        Log.d(TAG, "list.size():" +list.size());
        return list;
    }

    public List<AppText> getAppTextOnDB () {
        List<AppText> list = new ArrayList<>();
        c = db.query(DBHelper.APP_TEXT.TABLE_NAME, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int pkColIndex = c.getColumnIndex(DBHelper.APP_TEXT.PK);
            int takedaColIndex = c.getColumnIndex(DBHelper.APP_TEXT.TAKEDA);
            int warningColInder = c.getColumnIndex(DBHelper.APP_TEXT.WARNING);
            int agreementColIndex = c.getColumnIndex(DBHelper.APP_TEXT.AGREEMENT);
            do {
                list.add(new AppText(
                        c.getLong(pkColIndex),
                        c.getString(takedaColIndex),
                        c.getString(warningColInder),
                        c.getString(agreementColIndex)
                ));
            } while (c.moveToNext());
        } else {
            Log.d(TAG, "0_rows");
        }
        c.close();
        return list;
    }

    public void putConferenceList(List<Conference> list) {
        Log.d(TAG, "putConferenceList list.size():" + list.size());
        ContentValues cv = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            cv.put(DBHelper.TABLE_CONFERENCE.PK, list.get(i).getPk());
            cv.put(DBHelper.TABLE_CONFERENCE.TITLE, list.get(i).getTitle());
            cv.put(DBHelper.TABLE_CONFERENCE.START_DATE, list.get(i).getStartDate());
            cv.put(DBHelper.TABLE_CONFERENCE.END_DATE, list.get(i).getEndDate());
            cv.put(DBHelper.TABLE_CONFERENCE.CITY, list.get(i).getCity());
            cv.put(DBHelper.TABLE_CONFERENCE.ADDRESS, list.get(i).getAddress());
            cv.put(DBHelper.TABLE_CONFERENCE.DESC, list.get(i).getDescription());
            cv.put(DBHelper.TABLE_CONFERENCE.LOGO, list.get(i).getLogo());
            cv.put(DBHelper.TABLE_CONFERENCE.MAP, list.get(i).getMap());
            cv.put(DBHelper.TABLE_CONFERENCE.HALLS, list.get(i).getHalls());
            long rowID = db.insert(DBHelper.TABLE_CONFERENCE.TABLE_NAME, null, cv);
            Log.d(TAG, "row inserted Conference, ID = " + rowID);
            updateDB();
        }
    }

    public void putAppText(List<AppText> list) {
        ContentValues cv = new ContentValues();
        for (int i = 0; i < list.size(); i++) {
            cv.put(DBHelper.APP_TEXT.PK, list.get(i).getPk());
            cv.put(DBHelper.APP_TEXT.TAKEDA, list.get(i).getTakeda());
            cv.put(DBHelper.APP_TEXT.WARNING, list.get(i).getWarning());
            cv.put(DBHelper.APP_TEXT.AGREEMENT, list.get(i).getAgreement());
            long rowID = db.insert(DBHelper.APP_TEXT.TABLE_NAME, null, cv);
            Log.d(TAG, "row inserted Conference, ID = " + rowID);
            updateDB();
        }
    }

    private void updateDB() {
        dbHelper.close();
        db = dbHelper.getWritableDatabase();
    }

    public void deleteTable(String table) {
        db.delete(table, null, null);
    }



    /*private DBHelper dbHelper;
    private SQLiteDatabase dbase;
    private Cursor c;
    private static final String TAG = "MyTAG DataBase";

    public DataBase () {
    }

    public DataBase (Context context, String nameDataBase) {
        dbHelper = new DBHelper(context, nameDataBase);
        dbase = dbHelper.getWritableDatabase();
    }

    public boolean createTable(String nameTable, String[] nameColumns) {

        if (!isTableExists(nameTable)) {
            String command = "";
            for (int i = 0; i < nameColumns.length; i++) {
                if (i + 1 < nameColumns.length) {
                    command += nameColumns[i] + " text,";
                } else {
                    command += nameColumns[i] + " text";
                }
            }

            dbase.execSQL("create table " + nameTable
                    + " (" + command + ");");

            return true;
        } else {
            return false;
        }
    }

    public boolean isTableExists(String nameTable) {
        Cursor tableCursor = dbase.rawQuery("SELECT name FROM sqlite_master WHERE name='"
            + nameTable + "'", null);
        if (tableCursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEmpty(String nameTable) {

        if (isTableExists(nameTable)) {
            c = dbase.query(nameTable, null, null, null, null, null, null);

            if (c.moveToFirst()) {
                boolean flag = true;

                for (int j = 0; j < c.getColumnCount(); j++) {
                    if (c.isNull(j)) {
                        flag = false;
                    }
                }

                c.close();
                return flag;
            }
        }
        return true;
    }

    public boolean isFullEmpty(String nameTable) {

        if (isTableExists(nameTable)) {
            c = dbase.query(nameTable, null, null, null, null, null, null);

            if (c.moveToFirst()) {
                int countEmptyColumns = 0;
                int countColumns = c.getColumnCount();

                for (int j = 0; j < c.getColumnCount(); j++) {
                    if (c.isNull(j)) {
                        countEmptyColumns++;
                    }
                }

                c.close();
                return countEmptyColumns == countColumns ? true : false;
            }
        }

        return true;
    }

    public String getData(String nameTable, String columnTag) {

        if (isTableExists(nameTable)) {
            c = dbase.query(nameTable, null, null, null, null, null, null);

            if (c.moveToFirst()) {

                int columnIndex = c.getColumnIndex(columnTag);
                String Data = c.getString(columnIndex);
                c.close();

                return Data;
            } else {
                c.close();
                return null;
            }
        } else {
            return null;
        }
    }

    public String[] getData(String nameTable, String[] columnTag) {

        String[] data = new String[columnTag.length];

        for (int i = 0; i < columnTag.length; i++) {
            data[i] = getData(nameTable, columnTag[i]);
        }

        return data;
    }

    public boolean isDataEmpty(String nameTable, String columnTag) {
        return getData(nameTable, columnTag) == null;
    }

    public boolean putData(String nameTable, String[] columnTag, String[] data) {
        if (isTableExists(nameTable)) {
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < columnTag.length; i++) {
                contentValues.put(columnTag[i], data[i]);
            }
            dbase.insert(nameTable, null, contentValues);
            return true;
        } else {
            return false;
        }

    }

    public boolean replaceData(String nameTable, String[] columnTag, String[] putColumn, String[] data) {

        if (isTableExists(nameTable)) {
            ContentValues cv = new ContentValues();
            for (int i = 0, j = 0; i < columnTag.length; i++) {
                if (j != putColumn.length && columnTag[i].matches(putColumn[j])) {
                    cv.put(putColumn[j], data[j]);
                    j++;
                } else {
                    cv.put(columnTag[i], getData(nameTable, columnTag[i]));
                }
            }

            deleteTable(nameTable);
            dbase.insert(nameTable, null, cv);

            return true;
        } else {
            return false;
        }

    }

    public void openDataBase() {
        dbase = dbHelper.getWritableDatabase();
    }

    public void closeDataBase() {
        dbHelper.close();
    }

    public void updateDataBase() {
        dbHelper.close();
        dbase = dbHelper.getWritableDatabase();
    }

    public void deleteTable(String table) {
        dbase.delete(table, null, null);
    }
    //"ID", "ICON", "TITLE", "ADDRESS", "CITY", "COUNTRY", "DESCRIPTION", "DATE_START", "DATA_END"
    public void putConference(List<ConferenceItem> conferenceItem) {
        if (isFullEmpty(Settings.NAME_CONFERENCE_TABLE)) {
            for (int position = 0; position < conferenceItem.size(); position++) {
                putData(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE, new String[] {String.valueOf(conferenceItem.get(position).getId()),
                        conferenceItem.get(position).getIcon(),
                        conferenceItem.get(position).getTitle(),
                        conferenceItem.get(position).getAddress(),
                        conferenceItem.get(position).getCity(),
                        conferenceItem.get(position).getCountry(),
                        conferenceItem.get(position).getDescription(),
                        conferenceItem.get(position).getDate_start(),
                        conferenceItem.get(position).getDate_end()});
            }

        } else {
            deleteTable(Settings.NAME_CONFERENCE_TABLE);
            for (int position = 0; position < conferenceItem.size(); position++) {


                putData(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE, new String[] {String.valueOf(conferenceItem.get(position).getId()),
                        conferenceItem.get(position).getIcon(),
                        conferenceItem.get(position).getTitle(),
                        conferenceItem.get(position).getAddress(),
                        conferenceItem.get(position).getCity(),
                        conferenceItem.get(position).getCountry(),
                        conferenceItem.get(position).getDescription(),
                        conferenceItem.get(position).getDate_start(),
                        conferenceItem.get(position).getDate_end()});
            }
            updateDataBase();
        }
    }

    public List<String> getDataList(String TableName, String rowName) {
        c = dbase.query(TableName, null, null, null, null, null, null);
        List<String> list = new ArrayList<>();
        if (c.moveToFirst()) {
            int colIndex = c.getColumnIndex(rowName);
            do {
                list.add(c.getString(colIndex));
            } while (c.moveToPrevious());
        } else {
            Log.d(TAG, "0 row");
        }
        return list;
    }*/



    /*public List<ConferenceItem> getConferenceList() {
        Log.d(TAG, "getConferenceList Create");
        //"ID", "ICON", "TITLE", "ADDRESS", "CITY", "COUNTRY", "DESCRIPTION", "DATE_START", "DATA_END"
        List<String> listId = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[0]);
        List<String> listIcon = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[1]);
        List<String> listTitle = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[2]);
        List<String> listAddress = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[3]);
        List<String> listCity = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[4]);
        List<String> listCountry = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[5]);
        List<String> listDescription = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[6]);
        List<String> listDateStart = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[7]);
        List<String> listDateEnd = getDataList(Settings.NAME_CONFERENCE_TABLE, Settings.NAME_COLUMNS_CONFERENCE_TABLE[8]);
        List<ConferenceItem> list = new ArrayList<>();
        for (int i = 0; i < listId.size(); i++) {
            Log.d(TAG, "getConferenceList get date:" + i);
            list.add(new ConferenceItem(Long.valueOf(listId.get(i)),
                    listIcon.get(i),
                    listTitle.get(i),
                    listAddress.get(i),
                    listCity.get(i),
                    listCountry.get(i),
                    listDescription.get(i),
                    listDateStart.get(i),
                    listDateEnd.get(i)));
        }
        return list;
    }*/


}
