package com.example.demafayz.takedaconferencesuat.screens.conference.schedule.navigation;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleNavigationDrawer extends Fragment {


    public ScheduleNavigationDrawer() {
        // Required empty public constructor
    }

    private View containerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mUserLernedDrawer;
    private boolean mFromSavedInstanceState;
    private static final String PREF_FILE_NAME = "Schedule_Pref_File_Name";
    private static final String KEY_USER_LEARNED_DRAWER = "schedule_user_kay";
    private List<String> list = new ArrayList<>();

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigationDrawerOpen, R.string.navigationDrawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLernedDrawer) {
                    mUserLernedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLernedDrawer + "");
                }
                getActivity().invalidateOptionsMenu();
            }
        };

        if (!mUserLernedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.closeDrawer(containerView);
        } else {
            mDrawerLayout.openDrawer(containerView);
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private class ViewHolder {
        ListView lvScheduleList;
    }

    private ViewHolder vh = new ViewHolder();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_schedule_navigation_drawer, container, false);
        populateViewHolder(layout);
        return layout;
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(containerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    public ListView getListView() {
        return vh.lvScheduleList;
    }

    public void createNewAdapter(List<String> list) {
        ScheduleNavigationAdapter adapter = new ScheduleNavigationAdapter(getActivity(), 0, list);
        vh.lvScheduleList.setAdapter(adapter);
    }

    public void populateViewHolder(View layout) {
        vh.lvScheduleList = (ListView) layout.findViewById(R.id.lvScheduleSpis);
        /*ScheduleNavigationAdapter adapter = new ScheduleNavigationAdapter(getActivity(), 0, list);
        vh.lvScheduleList.setAdapter(adapter);*/
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }
}