package com.example.demafayz.takedaconferencesuat.api;

import android.util.Log;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.dataExamples.*;
import com.example.demafayz.takedaconferencesuat.screens.conference.schedule.ScheduleItem;

import com.google.api.client.util.Lists;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.demafayz.takedaconferencesuat.util.Constants.CLIENT_ID;
import static com.example.demafayz.takedaconferencesuat.util.Constants.CLIENT_SECRET;


/**
 * Created by demafayz on 02.09.15.
 */
public class ApiHelper {
    /*
     *  Example
     *
     * "drug": "http://master.takeda.huskyjam.com/api/v1/drug/"
     * "lecturer": "http://master.takeda.huskyjam.com/api/v1/lecturer/"
     * "conference": "http://master.takeda.huskyjam.com/api/v1/conference/"
     * "hall": "http://master.takeda.huskyjam.com/api/v1/hall/"
     * "symposium": "http://master.takeda.huskyjam.com/api/v1/symposium/"
     * "lecture": "http://master.takeda.huskyjam.com/api/v1/lecture/"
     * "text_page": "http://master.takeda.huskyjam.com/api/v1/text_page/"
     * "summary": "http://master.takeda.huskyjam.com/api/v1/summary/"
     *
     * */

    private static final String TAG = "MyTag ApiHelper";

    public static final String API_VERSION_1 = "v1/";
    public static final String BASE_URL = "http://master.takeda.huskyjam.com/api/";
    public static final String BASE_URL_TEST = "http://temporaryversionwithoutauth.takeda.huskyjam.com/api/";
    public static final String HOST_NAME = "http://master.takeda.huskyjam.com";

    public static final class ROOT_JSON_TEGS {
        public static final String LECTURERS = "lecturers";
        public static final String SYMPOSIUMS = "symposiums";
        public static final String TEXT_PAGES = "text_pages";
        public static final String CONFERENCES = "conferences";
        public static final String DRUGS = "drugs";
        public static final String LECTURE = "lectures";
    }

    public static final class PARAMS {
        public static final String DRUG = "drug/";
        public static final String LECTURER = "lecturer/";
        public static final String CONFERENCE = "conference/";
        public static final String HALL = "hall/";
        public static final String SYMPOSIUM = "symposium/";
        public static final String LECTURE = "lecture/";
        public static final String TEXT_PAGE = "text_page/";
        public static final String SUMMARY = "summary/";
    }

    public static final class ConfTAG {
        public static final String PK = "pk";
        public static final String TITLE = "title";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String CITY = "city";
        public static final String ADDRESS = "address";
        public static final String DESC = "description";
        public static final String LOGO = "logo";
        public static final String MAP = "map";
        public static final String HALLS = "halls";
    }

    public static final class LecturerTAGS {
        public static final String PK = "pk";
        public static final String NAME = "name";
        public static final String WORK = "work";
        public static final String SCIENTIFIC_TITLE = "scientific_title";
        public static final String SHOW_IN_LIST = "show_in_list";
        public static final String PHOTO = "photo";
        public static final String LECTURES = "lectures";
    }

    public static final class DrugTAGS {
        public static final String PK = "pk";
        public static final String NAME = "name";
        public static final String SHORT_DESCRIPTION = "short_description";
        public static final String DESCRIPTION = "description";
        public static final String IMAGE = "image";
    }

    public static final class TEXT_PAGE_TAGS {
        public static final String PK = "pk";
        public static final String TAKEDA = "takeda";
        public static final String WARNING = "warning";
        public static final String AGREEMENT = "agreement";
    }

    public static String getQuery(int apiVersion, String param) {
        switch (apiVersion) {
            case 1: {
                return HOST_NAME + "/api/v1/" + param;
            }
            default: {
                return null;
            }
        }
    }

    /*public interface ApiTokenInterface {
        @POST("/name={name}&password={password}&email={email}&profession={profession}")
        public UserToken getUserToken(@Path("name") String name,
                                      @Path("email") String email,
                                      @Path("password") String password,
                                      @Path("profession") String profession);
    }*/

    /*public static List<Conference> getJsonConference() {
        JSONObject question = null;
        JSONArray results = null;
        List<Conference> list = new ArrayList<>();
        ServiceHandler sh = new ServiceHandler();
        String jsonStr = sh.makeServiceCall(getQuery(1, PARAMS.CONFERENCE), ServiceHandler.GET);
        Log.d(TAG, "Response: > ");
        System.out.println(jsonStr);
        if (jsonStr != null) {
            try {
                question = new JSONObject(jsonStr);
                //Log.d(TAG, question.toString(1));
                results = question.getJSONArray("results");
                Log.d(TAG, String.valueOf(results.length()));
                for (int i = 0; i < results.length(); i++) {
                    JSONObject cursore = results.getJSONObject(i);
                    list.add(new Conference(
                            cursore.getInt(ConfTAG.PK),
                            cursore.getString(ConfTAG.START_DATE),
                            stringToDateLong(cursore.getString(ConfTAG.START_DATE), null),
                            stringToDateLong(cursore.getString(ConfTAG.END_DATE), null),
                            cursore.getString(ConfTAG.CITY),
                            cursore.getString(ConfTAG.ADDRESS),
                            cursore.getString(ConfTAG.DESC),
                            cursore.getString(ConfTAG.LOGO),
                            cursore.getString(ConfTAG.MAP),
                            null));
                }

            } catch (JSONException e) {
                Log.d(TAG, "ERR: getJsonConference");
            }
        }
        return list;
    }*/

    public static List<Conference> getJsonConferenceOnRootJson(JSONObject rootJson) {
        List<Conference> list = new ArrayList<>();
        JSONArray results = null;
        try {
            results = rootJson.getJSONArray(ROOT_JSON_TEGS.CONFERENCES);
            Log.d(TAG, "results Conference size: " + results.length());
            for (int i = 0; i < results.length(); i++) {
                JSONObject cursore = results.getJSONObject(i);
                String mapUrl = cursore.getString("map");
                list.add(new Conference(cursore.getInt("pk"),
                        cursore.getString("title"),
                        stringToDateLong(cursore.getString("start_date"), null),
                        stringToDateLong(cursore.getString("end_date"), null),
                        cursore.getString("city"),
                        cursore.getString("address"),
                        cursore.getString("description"),
                        cursore.getString("logo"),
                                mapUrl,
                        cursore.getJSONArray("halls").toString(),
                        parseMapPoints(cursore)));
                Log.d(TAG, "JSON Conf: " + cursore.getJSONArray("halls").toString(1));
                List<ScheduleItem> list1 = getConferenceHalls(cursore.getJSONArray("halls"), mapUrl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "Conference JSONException");
        }
        return list;
    }

    private static List<MapEntityPoint> parseMapPoints(JSONObject cursore) throws JSONException {
        JSONArray halls = cursore.getJSONArray("halls");
        List<MapEntityPoint> hallInfos = Lists.newArrayList();
        for (int i = 0; i < halls.length(); i++) {
            JSONObject o = halls.getJSONObject(i);
            hallInfos.add(new MapEntityPoint(
                    o.getInt("x_coord"),
                    o.getInt("y_coord"),
                    o.getString("title")
            ));
        }

        return hallInfos;
    }

    public static List<ScheduleItem> getConferenceHalls(JSONArray halls, String mapUrl) {
        List<ScheduleItem> list = new ArrayList<>();
        long startDate;
        long endDate;
        String room;
        String lectoreText;
        String title;
        String headerTitle;
        int headerId = 0;
        try {
            for (int i = 0; i < halls.length(); i++) {
                JSONObject hall = halls.getJSONObject(i);
                room = hall.getString("title");
                int x_coord = hall.getInt("x_coord");
                int y_coord = hall.getInt("y_coord");
                JSONArray symposiums = hall.getJSONArray("symposiums");
                for (int j = 0; j < symposiums.length(); j++) {
                    JSONObject symposium = symposiums.getJSONObject(j);
                    headerTitle = symposium.getString("title");
                    startDate = stringToDateLong(symposium.getString("date") + " " + symposium.getString("start_time"), "yyyy-MM-dd HH:mm:ss");
                    Log.d(TAG, "start time: " + symposium.getString("date") + " " + symposium.getString("start_time"));
                    endDate = stringToDateLong(symposium.getString("date") + " " + symposium.getString("end_time"), "yyyy-MM-dd HH:mm:ss");
                    JSONArray lectures = symposium.getJSONArray("lectures");
                    lectoreText = "";
                    headerId++;
                    Log.d(TAG, "Date: " + startDate + " " + endDate);
                    list.add(new ScheduleItem(headerTitle, lectoreText, startDate, endDate, room, headerId, true, x_coord, y_coord, mapUrl));
                    for (int z = 0; z < lectures.length(); z++) {
                        JSONObject lecture = lectures.getJSONObject(z);
                        title = lecture.getString("title");
                        try {
                            JSONObject lecturer = lecture.getJSONObject("lecturer");
                            lectoreText = lecturer.getString("name");
                        } catch (JSONException e) {
                            lectoreText = "null";
                        }
                        list.add(new ScheduleItem(title, lectoreText, startDate, endDate, room, headerId, false, x_coord, y_coord, mapUrl));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<AppText> getTextPagesOnRootJson(JSONObject rootJson) {
        List<AppText> list = new ArrayList<>();
        JSONArray results = null;
        try {
            results = rootJson.getJSONArray("text_pages");
            for (int i = 0; i < results.length(); i++) {
                JSONObject cursore = results.getJSONObject(i);
                list.add(new AppText(cursore.getInt("pk"),
                        cursore.getString("takeda"),
                        cursore.getString("warning"),
                        cursore.getString("agreement")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Drug> getDrugOnRootJson(JSONObject rootJson) {
        List<Drug> list = new ArrayList<>();
        JSONArray results = null;
        try {
            results = rootJson.getJSONArray("drugs");
            for (int i = 0; i < results.length(); i++) {
                JSONObject cursore = results.getJSONObject(i);
                list.add(new Drug(cursore.getLong("pk"),
                        cursore.getString("name"),
                        cursore.getString("short_description"),
                        cursore.getString("description"),
                        cursore.getString("image")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Lecturer> getLecturersOnRootJson(JSONObject rootJson) {
        List<Lecturer> list = new ArrayList<>();
        JSONArray results = null;
        try {
            results = rootJson.getJSONArray("lecturers");
            for (int i = 0; i < results.length(); i++) {
                JSONObject cursore = results.getJSONObject(i);
                list.add(new Lecturer(cursore.getLong("pk"),
                        cursore.getString("name"),
                        cursore.getString("work"),
                        cursore.getString("scientific_title"),
                        cursore.getBoolean("show_in_list"),
                        cursore.getString("photo"),
                        cursore.getJSONArray("lectures").toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Response processLogin(String[] params) {
        final FormEncodingBuilder builder = new FormEncodingBuilder()
                .add("username", params[0])
                .add("password", params[1])
                .add("client_id", CLIENT_ID)
                .add("client_secret", CLIENT_SECRET)
                .add("grant_type", "password");

        final Request req = new Request.Builder()
                .url(ApiHelper.BASE_URL + "o/token/")
                .post(builder.build())
                .build();

        try {
            return App.getHttpClient().newCall(req).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public static List<ScheduleItem> getConferenceHalls(String jsonStrHalls) {
        List<ScheduleItem> list = new ArrayList<>();
        JSONArray halls = null;
        JSONArray symposiums = null;
        String title;
        String date;
        String startDate;
        String endDate;
        try {
            halls = new JSONArray(halls);
            for (int i = 0; i < halls.length(); i++) {
                JSONObject cursore = halls.getJSONObject(i);
                title = cursore.getString("title");
                symposiums = cursore.getJSONArray("lectures");
                for (int j = 0; i < symposiums.length(); j++) {
                    JSONObject symposium = symposiums.getJSONObject(i);
                    date = symposium.getString("date");
                    title = symposium.getString("title");
                    startDate = symposium.getString("start_time");
                    endDate = symposium.getString("end_time");
                    Log.d(TAG, "title: " + title);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }*/

    /*public static List<Lecturer> getLecturers() {
        JSONObject question = null;
        JSONArray results = null;
        List<Lecturer> list = new ArrayList<>();
        ServiceHandler sh = new ServiceHandler();
        String jsonStr = sh.makeServiceCall(getQuery(1, PARAMS.LECTURER), ServiceHandler.GET);
        Log.d(TAG, "Response: > ");
        System.out.println(TAG + " " + jsonStr);
        if (jsonStr != null) {
            try {
                question = new JSONObject(jsonStr);
                results = question.getJSONArray("results");
                Log.d(TAG, String.valueOf(results.length()));
                for (int i = 0; i < results.length(); i++) {
                    JSONObject cursore = results.getJSONObject(i);
                    list.add(new Lecturer(
                            cursore.getLong(LecturerTAGS.PK),
                            cursore.getString(LecturerTAGS.NAME),
                            cursore.getString(LecturerTAGS.WORK),
                            cursore.getString(LecturerTAGS.SCIENTIFIC_TITLE),
                            cursore.getBoolean(LecturerTAGS.SHOW_IN_LIST),
                            cursore.getString(LecturerTAGS.PHOTO)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "ERR: JSONException");
            }
        }
        return list;
    }*/

    public static JSONObject getRootJson() {
        JSONObject question = null;
        ServiceHandler sh = new ServiceHandler();
        String jsonStr = sh.makeServiceCall(getQuery(1, PARAMS.SUMMARY), ServiceHandler.GET);
        Log.d(TAG, "Root JsonString" + jsonStr);
        try {
            question = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return question;
    }

    public static List<Drug> getDrugs() {
        JSONObject question = null;
        JSONArray results = null;
        List<Drug> list = new ArrayList<>();
        ServiceHandler sh = new ServiceHandler();
        String jsonStr = sh.makeServiceCall(getQuery(1, PARAMS.DRUG), ServiceHandler.GET);
        Log.d(TAG, "Response: > ");
        System.out.println(TAG + " " + jsonStr);
        if (jsonStr != null) {
            try {
                question = new JSONObject(jsonStr);
                results = question.getJSONArray("results");
                Log.d(TAG, String.valueOf(results.length()));
                for (int i = 0; i < results.length(); i++) {
                    JSONObject cursore = results.getJSONObject(i);
                    list.add(new Drug(
                            cursore.getLong(DrugTAGS.PK),
                            cursore.getString(DrugTAGS.NAME),
                            cursore.getString(DrugTAGS.SHORT_DESCRIPTION),
                            cursore.getString(DrugTAGS.DESCRIPTION),
                            cursore.getString(DrugTAGS.IMAGE)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(TAG, "ERR: getDrugs JSONException");
            }
        }
        return list;
    }

    public static List<AppText> loadAppTextDate() {
        JSONObject question = null;
        JSONArray results = null;
        List<AppText> list = new ArrayList<>();
        ServiceHandler sh = new ServiceHandler();
        String jsonStr = sh.makeServiceCall(getQuery(1, PARAMS.TEXT_PAGE), ServiceHandler.GET);
        Log.d(TAG, "Response: > ");
        System.out.println(TAG + " " + jsonStr);
        if (jsonStr != null) {
            try {
                question = new JSONObject(jsonStr);
                results = question.getJSONArray("results");
                Log.d(TAG, String.valueOf(results.length()));
                for (int i = 0; i < results.length(); i++) {
                    JSONObject cursore = results.getJSONObject(i);
                    list.add(new AppText(
                            cursore.getLong(TEXT_PAGE_TAGS.PK),
                            cursore.getString(TEXT_PAGE_TAGS.TAKEDA),
                            cursore.getString(TEXT_PAGE_TAGS.WARNING),
                            cursore.getString(TEXT_PAGE_TAGS.AGREEMENT)
                    ));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    public static long stringToDateLong(String strDate, String dateFormat) {
        //SimpleDateFormat formatter=new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        Date date = null;
        try {
            if (dateFormat == null || dateFormat.equals("")) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                date = formatter.parse(strDate);
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
                date = formatter.parse(strDate);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "ERR: stringToDateLong");
            return 0;
        }
        return date.getTime();
    }

    public static String longDateToString(long dateLong, String dateFormat) {
        Date date = new Date();
        date.setTime(dateLong);
        SimpleDateFormat formatter;
        if (dateFormat == null) {
            formatter = new SimpleDateFormat("yyyy-MM-dd");
        } else {
            formatter = new SimpleDateFormat(dateFormat);
        }

        String dateForm = formatter.format(date);
        return dateForm;
    }

    public static boolean sortConfer(long dateLong, String dateText, String dateExample) {
        long sortDateLong;
        long inpDateLong;
        String sortDateText;
        String inpDateText;
        Date sortDate = new Date();
        Date inpDate = new Date();
        Calendar cal = Calendar.getInstance();
        inpDateLong = stringToDateLong(dateText, dateExample);
        sortDateLong = dateLong;
        sortDate.setTime(sortDateLong);
        inpDate.setTime(inpDateLong);
        cal.setTime(sortDate);
        sortDateText = "" + cal.get(Calendar.DAY_OF_WEEK) + " " + cal.get(Calendar.MONTH);
        cal.setTime(inpDate);
        inpDateText = "" + cal.get(Calendar.DAY_OF_WEEK) + " " + cal.get(Calendar.MONTH);
        if (sortDateText.equals(inpDateText)) {
            return true;
        } else {
            return false;
        }
    }

    public static String longDateToString(long dateLong) {
        String[] mounch = {"января", "февраля", "марта", "апреля", "мая", "июня",
                "июля", "августа", "сентября", "октября", "ноября", "декабря"};
        Date date = new Date();
        date.setTime(dateLong);
        SimpleDateFormat formatter;
        //formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter = new SimpleDateFormat("yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return "" + calendar.get(Calendar.DAY_OF_WEEK) + " " + mounch[Calendar.WEEK_OF_YEAR] + " " + Calendar.YEAR;
    }

    public static DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols(){

        @Override
        public String[] getMonths() {
            return new String[]{"января", "февраля", "марта", "апреля", "мая", "июня",
                    "июля", "августа", "сентября", "октября", "ноября", "декабря"};
        }

    };
}
