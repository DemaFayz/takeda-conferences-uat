package com.example.demafayz.takedaconferencesuat.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.dataExamples.Lecturer;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.List;

/**
 * Created by demafayz on 03.09.15.
 */
public class LecturersAdapter extends ArrayAdapter<Lecturer> {

    private class ViewHolder{
        ImageView ivLecturerPhoto;
        TextView tvLecturerName;
        TextView tvLecturerWork;
    }
    private ViewHolder vh = new ViewHolder();

    private Context context;
    private List<Lecturer> list;
    private LayoutInflater inflater;

    public LecturersAdapter(Context context, int resource, List<Lecturer> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.lecturer_item, parent, false);
            vh.tvLecturerWork = (TextView) itemView.findViewById(R.id.tvLecturerWork);
            vh.tvLecturerName = (TextView) itemView.findViewById(R.id.tvLecturerName);
            vh.ivLecturerPhoto = (ImageView) itemView.findViewById(R.id.ivLecturerPhoto);
            itemView.setTag(vh);
        } else {
            vh = (ViewHolder) itemView.getTag();
        }
        UrlImageViewHelper.setUrlDrawable(vh.ivLecturerPhoto, list.get(position).getPhoto(), R.mipmap.ic_launcher, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        vh.tvLecturerName.setText(list.get(position).getName());
        vh.tvLecturerWork.setText(list.get(position).getScientificTitle());
        itemView.setTag(vh);
        return itemView;
    }*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.lecturer_item, parent, false);
        }
        vh.tvLecturerWork = (TextView) itemView.findViewById(R.id.tvLecturerWork);
        vh.tvLecturerName = (TextView) itemView.findViewById(R.id.tvLecturerName);
        vh.ivLecturerPhoto = (ImageView) itemView.findViewById(R.id.ivLecturerPhoto);

        UrlImageViewHelper.setUrlDrawable(vh.ivLecturerPhoto, list.get(position).getPhoto(), R.drawable.alpha_pix, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        vh.tvLecturerName.setText(list.get(position).getName());
        vh.tvLecturerWork.setText(list.get(position).getWork());
        return itemView;
    }
}
