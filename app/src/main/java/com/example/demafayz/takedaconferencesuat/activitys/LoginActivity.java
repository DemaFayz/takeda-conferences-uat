package com.example.demafayz.takedaconferencesuat.activitys;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.events.AuthProcessFinishedEvent;
import com.example.demafayz.takedaconferencesuat.fragments.SignInFragment;
import com.example.demafayz.takedaconferencesuat.login.logo.LoginFragment;
import com.example.demafayz.takedaconferencesuat.login.logo.LogoFragment;
import com.example.demafayz.takedaconferencesuat.util.Constants;

public class LoginActivity extends FragmentActivity {

    private SharedPreferences sp;
    private FragmentTransaction ft;
    private FragmentManager fm;
    private SignInFragment signInFragment;
    private LogoFragment logoFragment = new LogoFragment();
    private AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sp = getSharedPreferences(Constants.APPLICATION, MODE_PRIVATE);
        if (sp.getString(Constants.LOGIN_KEY, null) != null) {
            //Log.d("Login:", sp.getString(Constants.LOGIN_KEY, null));
            dialog = new AlertDialog.Builder(this);
            dialog.setTitle(null);
            dialog.setMessage(getResources().getString(R.string.not_net));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showLogo();
                }
            });
            showLogo();

        } else {
            showLogin();
            //Log.d("Login:", sp.getString(Constants.LOGIN_KEY, null));
        }


        App.bus().register(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    private void showLogo() {
        if (AppSupport.isNetworkAvailable(this)) {
            fm = getSupportFragmentManager();
            ft = fm.beginTransaction();
            ft.replace(R.id.login_fragment_container, logoFragment).commit();
        } else {
            dialog.show();
        }
    }

    private void showLogin() {
        signInFragment = SignInFragment.newInstance();
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.replace(R.id.login_fragment_container, signInFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(AuthProcessFinishedEvent e) {
        this.finish();
    }
}