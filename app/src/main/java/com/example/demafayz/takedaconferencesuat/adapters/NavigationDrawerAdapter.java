package com.example.demafayz.takedaconferencesuat.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.data.NavigationDrawerItem;

import java.util.List;

/**
 * Created by demafayz on 25.08.15.
 */
public class NavigationDrawerAdapter extends ArrayAdapter<NavigationDrawerItem> {

    private class ViewHolder {
        ImageView ivNavigationItemIcon;
        TextView tvNavigationItemTitle;
    }

    private ViewHolder vh = new ViewHolder();
    private List<NavigationDrawerItem> list;
    private Context context;
    private LayoutInflater inflater;
    private int selectPosition;
    private static final String TAG = "MyTag NavigationDrawerAdapter";

    public NavigationDrawerAdapter(Context context, int resource, List<NavigationDrawerItem> list, int selectPosition) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.selectPosition = selectPosition;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = this.inflater.inflate(R.layout.navigation_drawer_item, parent, false);
        vh.ivNavigationItemIcon = (ImageView) itemView.findViewById(R.id.ivNavigationItemIcon);
        vh.tvNavigationItemTitle = (TextView) itemView.findViewById(R.id.tvNavigationItemTitle);
        if (position == selectPosition) {
            Log.d(TAG, "selectPosition");
            vh.ivNavigationItemIcon.setImageResource(list.get(position).getIconSelect());
            vh.tvNavigationItemTitle.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
        } else {
            vh.ivNavigationItemIcon.setImageResource(list.get(position).getIconAnselect());
            vh.tvNavigationItemTitle.setTextColor(getContext().getResources().getColor(R.color.textColorPrimary));
        }
        vh.tvNavigationItemTitle.setText(list.get(position).getTitle());
        return itemView;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView Called" + " selectPosition:" + selectPosition + " position:" + position);
        View itemView = convertView;
        if (itemView == null) {
            itemView = this.inflater.inflate(R.layout.navigation_drawer_item, parent, false);
            vh.ivNavigationItemIcon = (ImageView) itemView.findViewById(R.id.ivNavigationItemIcon);
            vh.tvNavigationItemTitle = (TextView) itemView.findViewById(R.id.tvNavigationItemTitle);
            itemView.setTag(vh);
        } else {
            vh = (ViewHolder) itemView.getTag();
        }
        if (position == selectPosition) {
            Log.d(TAG, "selectPosition");
            vh.ivNavigationItemIcon.setImageResource(list.get(position).getIconSelect());

        } else {
            vh.ivNavigationItemIcon.setImageResource(list.get(position).getIconAnselect());
        }
        vh.tvNavigationItemTitle.setText(list.get(position).getTitle());
        itemView.setTag(vh);
        return itemView;
    }*/

    public void setSelectPosition(int selectPosition) {
        this.selectPosition = selectPosition;
    }
}
