package com.example.demafayz.takedaconferencesuat.dataExamples;

/**
 * Created by demafayz on 03.09.15.
 */
public class Drug {
    private long pk;
    private String name;
    private String shortDescription;
    private String description;
    private String image;

    public Drug(long pk, String name, String shortDescription, String description, String image) {
        this.pk = pk;
        this.name = name;
        this.shortDescription = shortDescription;
        this.description = description;
        this.image = image;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
