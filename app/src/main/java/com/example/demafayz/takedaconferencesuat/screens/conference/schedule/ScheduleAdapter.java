package com.example.demafayz.takedaconferencesuat.screens.conference.schedule;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.R;

import java.util.ArrayList;
import java.util.List;

import com.example.demafayz.takedaconferencesuat.dataExamples.MapEntityPoint;
import com.example.demafayz.takedaconferencesuat.screens.conference.map.MapActivity;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.example.demafayz.takedaconferencesuat.util.MapActivityExtraJson;
import com.google.api.client.util.Lists;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by demafayz on 27.08.15.
 */
public class ScheduleAdapter extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {

    private final Context mContext;
    private List<ScheduleItem> mCountries;
    private List<Integer> mSectionIndices;
    private long[] mSectionLetters;
    private LayoutInflater mInflater;

    public ScheduleAdapter(Context context, List<ScheduleItem> list) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mCountries = list;
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }

    private List<Integer> getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        long lastFirstChar = mCountries.get(0).getHeaderId();
        sectionIndices.add(0);
        for (int i = 1; i < mCountries.size(); i++) {
            if (mCountries.get(i).getHeaderId() != (lastFirstChar)) {
                lastFirstChar = mCountries.get(i).getHeaderId();
                sectionIndices.add(i);
            }
        }
        List<Integer> sections = new ArrayList<>();
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections.add(sectionIndices.get(i));
        }
        return sections;
    }

    private long[] getSectionLetters() {
        long[] letters = new long[mSectionIndices.size()];
        for (int i = 0; i < mSectionIndices.size(); i++) {
            letters[i] = mCountries.get(mSectionIndices.get(i)).getHeaderId();
        }
        return letters;
    }

    @Override
    public int getCount() {
        return mCountries.size();
    }

    @Override
    public Object getItem(int position) {
        return mCountries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        holder = new ViewHolder();
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.schedule_list_item, parent, false);
        }
        holder.text = (TextView) convertView.findViewById(R.id.tvScheduleItem);
        //holder.lecturer = (TextView) convertView.findViewById(R.id.tvScheduleLecturer);
        holder.text.setText(mCountries.get(position).getTitle());
        //holder.lecturer.setText(mCountries.get(position).getLecturer());
        if (mCountries.get(position).isHeader()) {
            holder.text.setTypeface(Typeface.DEFAULT_BOLD);
            holder.text.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        } else {
            holder.text.setTypeface(Typeface.DEFAULT);
            holder.text.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        }

        return convertView;

        /*if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.schedule_list_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.tvScheduleItem);
            holder.lecturer = (TextView) convertView.findViewById(R.id.tvScheduleLecturer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText(mCountries.get(position).getTitle());
        holder.lecturer.setText(mCountries.get(position).getLecturer());
        if (mCountries.get(position).isHeader()) {
            holder.text.setTypeface(Typeface.DEFAULT_BOLD);
            holder.text.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        return convertView;*/
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.schedule_header_list_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.tvScheduleHeaderTitle);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        final ScheduleItem scheduleItem = mCountries.get(position);
        String headerTitle = scheduleItem.getDateHeader();
        holder.text.setText(headerTitle);
        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapActivity.class);

                MapEntityPoint point = new MapEntityPoint(scheduleItem.getX_coord(), scheduleItem.getY_coord(), scheduleItem.getRoom());
                List<MapEntityPoint> points = Lists.newArrayList();
                points.add(point);
                intent.putExtra(Constants.MAP_EXTRA_KEY,
                        App.gson().toJson(new MapActivityExtraJson(scheduleItem.getMapBackgroundUrl(), points)));

                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView text;
        TextView lecturer;
    }

    @Override
    public long getHeaderId(int position) {
        return mCountries.get(position).getHeaderId();
    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.size() == 0) {
            return 0;
        }

        if (section >= mSectionIndices.size()) {
            section = mSectionIndices.size() - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices.get(section);
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.size(); i++) {
            if (position < mSectionIndices.get(i)) {
                return i - 1;
            }
        }
        return mSectionIndices.size() - 1;
    }

    @Override
    public Object[] getSections() {
        return null;
    }

    public void clear() {
        mCountries.clear();
        mSectionIndices.clear();
        mSectionLetters = new long[0];
        notifyDataSetChanged();
    }

    public void restore(List<ScheduleItem> list) {
        mCountries = list;
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        notifyDataSetChanged();
    }

    /*private final Context mContext;
    private List<ScheduleItem> mCountries;
    private List<Integer> mSectionIndices;
    private long[] mSectionLetters;
    public List<String> list;
    private LayoutInflater mInflater;

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView text;
        TextView lecturer;
    }

    public ScheduleAdapter(Context context, List<ScheduleItem> list) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mCountries = list;
        this.mSectionIndices = getSectionIndices();
        this.mSectionLetters = getSectionLetters();
    }

    private List<Integer> getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        long lastFirstChar = mCountries.get(0).getDateStart();
        sectionIndices.add(0);
        for (int i = 1; i < mCountries.size(); i++) {
            if (mCountries.get(i).getDateStart() != lastFirstChar) {
                lastFirstChar = mCountries.get(i).getDateStart();
                sectionIndices.add(i);
            }
        }
        List<Integer> sections = new ArrayList<>();
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections.add(sectionIndices.get(i));
        }
        return sections;
    }

    private long[] getSectionLetters() {
        long[] letters = new long[mSectionIndices.size()];
        for (int i = 0; i < mSectionIndices.size(); i++) {
            letters[i] = mCountries.get(mSectionIndices.get(i)).getDateStart();
        }
        return letters;
    }

    @Override
    public int getCount() {
        return mCountries.size();
    }

    @Override
    public Object getItem(int position) {
        return mCountries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.schedule_list_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.tvScheduleItem);
            holder.lecturer = (TextView) convertView.findViewById(R.id.tvScheduleLecturer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(mCountries.get(position).getTitle());
        holder.lecturer.setText(mCountries.get(position).getLecturer());
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.schedule_header_list_item, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.tvScheduleHeaderTitle);

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        String headerTitle = mCountries.get(position).getDateHeader();
        holder.text.setText(headerTitle);
        notifyDataSetChanged();
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return mCountries.get(position).getDateStart();
    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.size() == 0) {
            return 0;
        }

        if (section >= mSectionIndices.size()) {
            section = mSectionIndices.size() - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices.get(section);
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.size(); i++) {
            if (position < mSectionIndices.get(i)) {
                return i - 1;
            }
        }
        return mSectionIndices.size() - 1;
    }

    @Override
    public Object[] getSections() {
        return null;
    }

    public void clear() {
        mCountries.clear();
        mSectionIndices.clear();
        mSectionLetters = new long[0];
        notifyDataSetChanged();
    }

    public void restore(List<ScheduleItem> list) {
        mCountries = list;
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        notifyDataSetChanged();
    }*/
}
