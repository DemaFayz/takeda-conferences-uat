package com.example.demafayz.takedaconferencesuat.data;

/**
 * Created by demafayz on 25.08.15.
 */
public class NavigationDrawerItem {
    private int iconSelect;
    private int iconAnselect;
    private String title;

    public NavigationDrawerItem(int iconSelect, int iconAnselect, String title) {
        this.iconSelect = iconSelect;
        this.iconAnselect = iconAnselect;
        this.title = title;
    }

    public int getIconSelect() {
        return iconSelect;
    }

    public void setIconSelect(int iconSelect) {
        this.iconSelect = iconSelect;
    }

    public int getIconAnselect() {
        return iconAnselect;
    }

    public void setIconAnselect(int iconAnselect) {
        this.iconAnselect = iconAnselect;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
