package com.example.demafayz.takedaconferencesuat.dataExamples;

import java.util.LinkedList;
import java.util.List;

public class Profession {

    private final String title;
    private final int id;

    public Profession(String title, int id) {
        this.title = title;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

}
