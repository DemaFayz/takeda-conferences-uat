package com.example.demafayz.takedaconferencesuat.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.demafayz.takedaconferencesuat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecoveryPassFragment extends Fragment implements View.OnClickListener {
    
    private ViewHolder vh = new ViewHolder();
    private FragmentActivity activity;

    private class ViewHolder {
        EditText etEmail;
        Button btnRecovery;
        Button btnBack;
    }

    public RecoveryPassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_recovery_pass, container, false);
        initData();
        populateViewHolder(layout);
        return layout;
    }

    private void populateViewHolder(View layout) {
        vh.etEmail = (EditText) layout.findViewById(R.id.et_recovery_pass);
        vh.btnRecovery = (Button) layout.findViewById(R.id.btn_recovery_pass);
        vh.btnRecovery.setOnClickListener(this);
        vh.btnBack = (Button) layout.findViewById(R.id.btn_back);
        vh.btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_recovery_pass:
                recoveryPass();
                break;
            case R.id.btn_back:
                activity.onBackPressed();
                break;
        }
    }

    private void recoveryPass() {
        String mail = vh.etEmail.getText().toString();
        //TODO Тут написать метод для восстановления пароли
    }

    private void initData() {
        this.activity = getActivity();
    }

    public static RecoveryPassFragment newInstance() {

        Bundle args = new Bundle();

        RecoveryPassFragment fragment = new RecoveryPassFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
