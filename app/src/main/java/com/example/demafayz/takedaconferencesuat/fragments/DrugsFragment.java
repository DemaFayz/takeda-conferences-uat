package com.example.demafayz.takedaconferencesuat.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.demafayz.takedaconferencesuat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DrugsFragment extends Fragment {


    public DrugsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drugs, container, false);
    }


}
