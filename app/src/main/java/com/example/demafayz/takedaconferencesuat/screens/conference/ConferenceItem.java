package com.example.demafayz.takedaconferencesuat.screens.conference;

/**
 * Created by demafayz on 26.08.15.
 */
public class ConferenceItem {

    //"ID", "ICON", "TITLE", "ADDRESS", "CITY", "COUNTRY", "DESCRIPTION", "DATE_START", "DATA_END"

    private long id;
    private String icon;
    private String title;
    private String address;
    private String city;
    private String country;
    private String description;
    private String date_start;
    private String date_end;

    public ConferenceItem(long id, String icon, String title, String address, String city, String country, String description, String date_start, String date_end) {
        this.id = id;
        this.icon = icon;
        this.title = title;
        this.address = address;
        this.city = city;
        this.country = country;
        this.description = description;
        this.date_start = date_start;
        this.date_end = date_end;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
}
