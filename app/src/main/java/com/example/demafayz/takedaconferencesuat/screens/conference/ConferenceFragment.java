package com.example.demafayz.takedaconferencesuat.screens.conference;


import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.dataBase.DataBase;
import com.example.demafayz.takedaconferencesuat.dataExamples.Conference;

import java.util.ArrayList;
import java.util.List;

import static com.example.demafayz.takedaconferencesuat.dataBase.DBHelper.TABLE_CONFERENCE.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConferenceFragment extends Fragment {

    private class ViewHolder {
        ListView lvConference;
    }

    private ViewHolder vh = new ViewHolder();
    private static List<Conference> listConference;
    private ConferenceAdapter adapter;
    private static final String TAG = "MyTag ConferenceFragment";
    private SearchView searchView;
    private long maxStart, minStart;
    public static String title;
    private Activity mActivity;
    private Thread thread;
    //---------------------

    public ConferenceFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_conference, container, false);
        populateViewHolder(layout);
        //new GetConf().execute();
        initClickRegister();
        if (title != null) {
            MainActivity.setTitle(title);
        }
        if (MainActivity.rootJson != null) {
            //createNewAdapter();
            new TaskNewAdapter().execute();
        }
        return layout;
    }

    private class TaskNewAdapter extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (listConference == null) {
                listConference = ApiHelper.getJsonConferenceOnRootJson(MainActivity.rootJson);
                if (title == null) {
                    MainActivity.setTitle(title = getMaxMinDate(listConference));
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (listConference != null) {
                adapter = new ConferenceAdapter(mActivity, 0, listConference);
                vh.lvConference.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void initClickRegister() {
        vh.lvConference.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ConferenceActivity.class);
                Conference conference = listConference.get(position);
                intent.putExtra(PK, conference.getPk());
                intent.putExtra(TITLE, conference.getTitle());
                intent.putExtra(START_DATE, conference.getStartDate());
                intent.putExtra(END_DATE, conference.getEndDate());
                intent.putExtra(CITY, conference.getCity());
                intent.putExtra(ADDRESS, conference.getAddress());
                intent.putExtra(DESC, conference.getDescription());
                intent.putExtra(LOGO, conference.getLogo());
                intent.putExtra(MAP, conference.getMap());
                intent.putExtra(HALLS, conference.getHalls());
                intent.putExtra(MAP_ENTITIES, App.gson().toJson(conference.getMapPoints()));

                startActivity(intent);
            }
        });
    }

    public void createNewAdapter() {
        if (listConference == null) {
            listConference = ApiHelper.getJsonConferenceOnRootJson(MainActivity.rootJson);
            if (title == null) {
                MainActivity.setTitle(title = getMaxMinDate(listConference));
            }

        }
        if (listConference != null) {
            adapter = new ConferenceAdapter(mActivity, 0, listConference);
            vh.lvConference.setAdapter(adapter);
        }
    }

    private String getMaxMinDate(List<Conference> list) {
        minStart = list.get(0).getStartDate();
        maxStart = list.get(0).getEndDate();
        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).getEndDate() > maxStart) {
                maxStart = list.get(i).getStartDate();
            }
            if (list.get(i).getStartDate() < minStart) {
                minStart = list.get(i).getStartDate();
            }
        }
        String title = ApiHelper.longDateToString(minStart, "d") + "-" + ApiHelper.longDateToString(maxStart, "d MMMM");
        return title;
    }

    private void populateViewHolder(View layout) {
        vh.lvConference = (ListView) layout.findViewById(R.id.lvConference);
    }

    private class GetConf extends AsyncTask<Void, Void, Void> {
        private List<Conference> list = new ArrayList<>();
        ProgressDialog dialog;
        DataBase dataBase;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Выполняется запрос");
            dialog.setCancelable(false);
            dialog.show();
            dataBase = new DataBase(ConferenceFragment.this.getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            //list = ApiHelper.getJsonConferenceOnSymmory(MainActivity.rootJson);
            dataBase.putConferenceList(list);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new DataBase(ConferenceFragment.this.getActivity()).putConferenceList(list);
            listConference = list;
            createNewAdapter();
            dialog.cancel();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_conference, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getActivity().SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        super.onPrepareOptionsMenu(menu);
    }
}
