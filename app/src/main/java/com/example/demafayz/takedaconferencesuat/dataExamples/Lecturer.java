package com.example.demafayz.takedaconferencesuat.dataExamples;

import java.util.List;

/**
 * Created by demafayz on 02.09.15.
 */
public class Lecturer {
    private long pk;
    private String name;
    private String work;
    private String scientificTitle;
    private boolean showInList;
    private String photo;
    private String lectures;

    public Lecturer(long pk, String name, String work, String scientificTitle, boolean showInList, String photo, String lectures) {
        this.pk = pk;
        this.name = name;
        this.work = work;
        this.scientificTitle = scientificTitle;
        this.showInList = showInList;
        this.photo = photo;
        this.lectures = lectures;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getScientificTitle() {
        return scientificTitle;
    }

    public void setScientificTitle(String scientificTitle) {
        this.scientificTitle = scientificTitle;
    }

    public boolean isShowInList() {
        return showInList;
    }

    public void setShowInList(boolean showInList) {
        this.showInList = showInList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLectures() {
        return lectures;
    }

    public void setLectures(String lectures) {
        this.lectures = lectures;
    }
}
