package com.example.demafayz.takedaconferencesuat.login.logo;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.login.LoginAsyncTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements Button.OnClickListener, TextWatcher {


    public LoginFragment() {
        // Required empty public constructor
    }

    private static final String TAG = "LoginFragment";

    private class ViewHolder {
        EditText etLogin;
        EditText etPass;
        Button btnRegis;
        Button btnLogin;
        //Button btnRestore;
    }

    private ViewHolder vh = new ViewHolder();
    private RegisFragment regisFragment;
    public static LogoFragment logoFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_sign_in, container, false);
        populateViewHolder(layout);
        clickedRegister();
        return layout;
    }

    private void clickedRegister() {
        vh.btnRegis.setOnClickListener(this);
        vh.btnLogin.setOnClickListener(this);
    }

    private void populateViewHolder(View layout) {
        vh.etLogin = (EditText) layout.findViewById(R.id.et_email);
        vh.etPass = (EditText) layout.findViewById(R.id.et_pass);
        vh.btnRegis = (Button) layout.findViewById(R.id.btn_sign_up);
        vh.btnLogin = (Button) layout.findViewById(R.id.btn_sign_in);
        vh.etLogin.addTextChangedListener(this);
        vh.etPass.addTextChangedListener(this);
        vh.btnLogin.setVisibility(View.GONE);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Log.d(TAG, "beforeTextChanged Called");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d(TAG, "onTextChanged Called");
    }

    @Override
    public void afterTextChanged(Editable s) {
        Log.d(TAG, "afterTextChanged Called");
        showLogBtn();
    }

    public void showLogBtn() {
        if ((vh.etLogin.getText().length() == 0) || (vh.etPass.getText().length() == 0)) {
            vh.btnLogin.setVisibility(View.GONE);
        } else {
            vh.btnLogin.setVisibility(View.VISIBLE);
        }
    }




    @Override
    public void onClick(View v) {
        if (AppSupport.isNetworkAvailable(getActivity())) {
            if (v.getId() == R.id.btn_sign_up) {
                regisFragment = new RegisFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.login_fragment_container, regisFragment).commit();
            } else if (R.id.btn_sign_in == v.getId()) {
                String login = vh.etLogin.getText().toString();
                String pass = vh.etPass.getText().toString();
                if (!login.isEmpty() && !pass.isEmpty()) {
                    new LoginAsyncTask(getActivity().getSupportFragmentManager(), getActivity()).execute(login, pass);
                }
            }
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage(getActivity().getResources().getString(R.string.not_net));
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }
}