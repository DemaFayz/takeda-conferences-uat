package com.example.demafayz.takedaconferencesuat.screens.conference.map;

import android.graphics.Point;

import java.util.List;

/**
 * Created by demafayz on 10.09.15.
 */
public class MapData {
    private String url;
    private List<Point> points;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
