package com.example.demafayz.takedaconferencesuat.dataExamples;

import java.util.List;

/**
 * Created by demafayz on 02.09.15.
 */
public class Conference {
    private long pk;
    private String title;
    private long startDate;
    private long endDate;
    private String city;
    private String address;
    private String description;
    private String logo;
    private String map;
    private String halls;
    private List<MapEntityPoint> mapPoints;

    public Conference(long pk, String title, long startDate, long endDate, String city, String address,
                      String description, String logo, String map, String halls, List<MapEntityPoint> mapPoints) {
        this.pk = pk;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.city = city;
        this.address = address;
        this.description = description;
        this.logo = logo;
        this.map = map;
        this.halls = halls;
        this.mapPoints = mapPoints;
    }

    public List<MapEntityPoint> getMapPoints() {
        return mapPoints;
    }

    public String getHalls() {
        return halls;
    }

    public void setHalls(String halls) {
        this.halls = halls;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
