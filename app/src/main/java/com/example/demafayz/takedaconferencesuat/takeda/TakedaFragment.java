package com.example.demafayz.takedaconferencesuat.takeda;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TakedaFragment extends Fragment {

    public TakedaFragment() {
        // Required empty public constructor
    }

    private class ViewHolder {
        TextView tvTakedaText;
    }

    private ViewHolder vh = new ViewHolder();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_takeda, container, false);
        populateViewHolder(layout);
        MainActivity.setTitle("Takeda");
        return layout;
    }

    private void populateViewHolder(View layout) {
        vh.tvTakedaText = (TextView) layout.findViewById(R.id.tvTakedaText);
        vh.tvTakedaText.setMovementMethod(LinkMovementMethod.getInstance());
        vh.tvTakedaText.setText(MainActivity.listAppText.get(MainActivity.listAppText.size() - 1).getTakeda());
    }


}