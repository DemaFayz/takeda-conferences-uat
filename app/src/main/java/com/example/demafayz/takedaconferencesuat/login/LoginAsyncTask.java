package com.example.demafayz.takedaconferencesuat.login;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;
import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.api.model.AuthResponseJson;
import com.example.demafayz.takedaconferencesuat.dialog.ProgressDialog;
import com.example.demafayz.takedaconferencesuat.events.AuthProcessFinishedEvent;
import com.example.demafayz.takedaconferencesuat.login.logo.LoginFragment;
import com.example.demafayz.takedaconferencesuat.login.logo.LogoFragment;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import static com.example.demafayz.takedaconferencesuat.util.Constants.*;

public class LoginAsyncTask extends AsyncTask<String, Void, Boolean> {
    private final FragmentManager fm;
    private Context context;
    ProgressDialog progressDialog;
    public LoginAsyncTask(FragmentManager fm) {
        this.fm = fm;
    }

    public LoginAsyncTask(FragmentManager fm, Context context) {
        this.fm = fm;
        progressDialog = new ProgressDialog(context);
    }

        @Override
        protected void onPreExecute() {
            //todo show status
            /*LogoFragment fragment = new LogoFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(SHOULD_LOOP_KEY, true);
            fragment.setArguments(bundle);
            fm.beginTransaction().replace(R.id.rlLoginContainer, fragment)
                    .commitAllowingStateLoss();*/
            if (progressDialog != null) {
                progressDialog.show();
            }
        }

        @Override
        protected void onPostExecute(Boolean succeed) {
            if (!succeed) {
                Toast.makeText(App.get(), "Ошибка при авторизации", Toast.LENGTH_SHORT).show();
                fm.beginTransaction()
                        .replace(R.id.login_fragment_container, new LoginFragment())
                        .commitAllowingStateLoss();
            } else {
                App.bus().post(new AuthProcessFinishedEvent());
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            final FormEncodingBuilder builder = new FormEncodingBuilder()
                    .add("username", params[0])
                    .add("password", params[1])
                    .add("client_id", CLIENT_ID)
                    .add("client_secret", CLIENT_SECRET)
                    .add("grant_type", "password");

            final Request req = new Request.Builder()
                    .url(ApiHelper.BASE_URL + "o/token/")
                    .post(builder.build())
                    .build();

            try {
                Response response = App.getHttpClient().newCall(req).execute();
                if (response.isSuccessful()) {
                    AuthResponseJson responseJson = new Gson()
                            .fromJson(response.body().string(), AuthResponseJson.class);

                    String at = responseJson.getAccess_token();


                    App.get().getSharedPreferences(APPLICATION, Context.MODE_PRIVATE)
                            .edit()
                            .putString(API_TOKEN_KEY, at)
                            .putString(LOGIN_KEY, params[0])
                            .putString(PASSWORD_KEY, params[1])
                            .commit();
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }