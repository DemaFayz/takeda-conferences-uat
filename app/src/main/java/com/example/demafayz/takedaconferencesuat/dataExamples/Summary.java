package com.example.demafayz.takedaconferencesuat.dataExamples;

import java.util.List;

/**
 * Created by demafayz on 02.09.15.
 */
public class Summary {
    List<Lecturer> lecturers;
    List<Symposium> symposiums;
    List<String> textPages;
}
