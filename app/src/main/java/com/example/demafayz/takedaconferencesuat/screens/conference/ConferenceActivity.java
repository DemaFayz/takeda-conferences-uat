package com.example.demafayz.takedaconferencesuat.screens.conference;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.dataBase.DBHelper;
import com.example.demafayz.takedaconferencesuat.dataExamples.Conference;
import com.example.demafayz.takedaconferencesuat.dataExamples.MapEntityPoint;
import com.example.demafayz.takedaconferencesuat.screens.conference.map.MapActivity;
import com.example.demafayz.takedaconferencesuat.screens.conference.schedule.ScheduleActivity;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.example.demafayz.takedaconferencesuat.util.MapActivityExtraJson;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.LinkedList;
import java.util.List;

import static com.example.demafayz.takedaconferencesuat.dataBase.DBHelper.TABLE_CONFERENCE.*;

public class ConferenceActivity extends ActionBarActivity {

    private class ViewHolder {
        ImageView ivActivityConferenceIcon;
        TextView tvActivityConferenceTitle;
        TextView tvActivityConferenceCity;
        TextView tvActivityConferenceAddress;
        TextView tvActivityConferenceDescription;
        TextView tvAddress;
        Button btnConferenceSchedule;
    }

    private ViewHolder vh = new ViewHolder();
    public static Conference conferenceItem;
    private Toolbar toolbar;
    private boolean flagCh = true;
    private static final String TAG = "MyTag ConferenceActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conference);
        populateViewHolder();
        initData();
        initView();
        onClickRegister();
    }

    private void onClickRegister() {
        vh.btnConferenceSchedule.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagCh = true;
                Intent intent = new Intent(ConferenceActivity.this, ScheduleActivity.class);
                intent.putExtra(HALLS, conferenceItem.getHalls());
                intent.putExtra(MAP_ENTITIES, App.gson().toJson(conferenceItem.getMapPoints()));
                startActivity(intent);
            }
        });
    }

    private void populateActionBar(String title) {
        toolbar = (Toolbar) findViewById(R.id.appBar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initView() {
        UrlImageViewHelper.setUrlDrawable(vh.ivActivityConferenceIcon, conferenceItem.getLogo(), R.mipmap.ic_launcher, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        Log.d(TAG, "Conference Title:" + conferenceItem.getTitle());
        vh.tvActivityConferenceTitle.setText(conferenceItem.getTitle());
        vh.tvActivityConferenceCity.setText(ApiHelper.longDateToString(conferenceItem.getStartDate(), "MMMM yyyy"));
        vh.tvActivityConferenceAddress.setText(conferenceItem.getCity());
        vh.tvAddress.setText(conferenceItem.getAddress());
        vh.tvActivityConferenceDescription.setText(conferenceItem.getDescription());
    }

    private void initData() {
        Intent intent = getIntent();

        if (intent.hasExtra(PK)) {
            long pk = intent.getLongExtra(PK, -1);
            String title = intent.getStringExtra(TITLE);
            long startDate = intent.getLongExtra(START_DATE, -1);
            long endDate = intent.getLongExtra(END_DATE, -1);
            String city = intent.getStringExtra(CITY);
            String address = intent.getStringExtra(ADDRESS);
            String desc = intent.getStringExtra(DESC);
            String logo = intent.getStringExtra(LOGO);
            String map = intent.getStringExtra(MAP);
            String halls = intent.getStringExtra(HALLS);
            List<MapEntityPoint> mapPoints = MapEntityPoint.fromJsonList(intent.getStringExtra(MAP_ENTITIES));

            conferenceItem = new Conference(pk, title, startDate, endDate, city, address, desc, logo, map, halls, mapPoints);
        }

        String title = ApiHelper.longDateToString(conferenceItem.getStartDate(), "d") + "-" + ApiHelper.longDateToString(conferenceItem.getEndDate(), "d MMMM");
        populateActionBar(title);
    }


    private void populateViewHolder() {
        vh.ivActivityConferenceIcon = (ImageView) findViewById(R.id.ivActivityConferenceIcon);
        vh.tvActivityConferenceTitle = (TextView) findViewById(R.id.tvActivityConferenceTitle);
        vh.tvActivityConferenceCity = (TextView) findViewById(R.id.tvActivityConferenceCity);
        vh.tvActivityConferenceAddress = (TextView) findViewById(R.id.tvActivityConferenceAddress);
        vh.tvActivityConferenceDescription = (TextView) findViewById(R.id.tvActivityConferenceDescription);
        vh.tvAddress = (TextView) findViewById(R.id.tvConfCity);
        vh.btnConferenceSchedule = (Button) findViewById(R.id.btnConferenceSchedule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_conf_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.menu_map) {
            Intent intent = new Intent(ConferenceActivity.this, MapActivity.class);
            List<MapEntityPoint> events = conferenceItem.getMapPoints();
            MapActivityExtraJson json = new MapActivityExtraJson(conferenceItem.getMap(), events);
            intent.putExtra(Constants.MAP_EXTRA_KEY, App.gson().toJson(json));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}