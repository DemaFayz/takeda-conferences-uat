package com.example.demafayz.takedaconferencesuat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.demafayz.takedaconferencesuat.dataExamples.Drug;
import com.example.demafayz.takedaconferencesuat.dataExamples.Lecturer;
import com.example.demafayz.takedaconferencesuat.screens.conference.schedule.ScheduleItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by demafayz on 10.09.15.
 */
public class AppSupport {
    public static List<ScheduleItem> sortScheduleList(List<ScheduleItem> patient, String text) {
        List<ScheduleItem> result = new ArrayList<>();
        for (int i = 0; i < patient.size(); i++) {
            if (sortScheduleItem(patient.get(i), text)) {
                result.add(patient.get(i));
            }
        }
        return result;
    }

    private static boolean sortScheduleItem(ScheduleItem patient, String text) {
        String query = patient.getTitle();
        if (query.indexOf(text) != -1) {
            return true;
        } else {
            return false;
        }
    }

    public static List<Drug> sortDrugsList(List<Drug> patient, String text) {
        List<Drug> result = new ArrayList<>();
        for (int i = 0; i < patient.size(); i++) {
            if (sortDrugItem(patient.get(i), text)) {
                result.add(patient.get(i));
            }
        }
        return result;
    }

    private static boolean sortDrugItem(Drug patient, String text) {
        String query = patient.getName() + patient.getDescription();
        if (query.indexOf(text) != -1) {
            return true;
        } else {
            return false;
        }
    }

    //Metod dlya proverki interneta
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static List<Lecturer> sortLecturer(List<Lecturer> patient, String text) {
        List<Lecturer> result = new ArrayList<>();
        for (int i = 0; i < patient.size(); i++) {
            if (sortLecturerItem(patient.get(i), text)) {
                result.add(patient.get(i));
            }
        }
        return result;
    }

    private static boolean sortLecturerItem(Lecturer patient, String text) {
        String query = patient.getName() + patient.getWork();
        if (query.indexOf(text) != -1) {
            return true;
        } else {
            return false;
        }
    }
}
