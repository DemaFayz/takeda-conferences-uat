package com.example.demafayz.takedaconferencesuat.login;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;
import com.example.demafayz.takedaconferencesuat.App;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.api.UserToken;
import com.example.demafayz.takedaconferencesuat.login.logo.LoginFragment;
import com.example.demafayz.takedaconferencesuat.login.logo.LogoFragment;
import com.example.demafayz.takedaconferencesuat.util.Constants;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class ProcessRegistrationAsyncTask extends AsyncTask<String, Void, Boolean> {
        private FragmentManager fm;
        private String message;
        private String logErr;
        private String emailErr;
        private String userName;
        private String userPass;

        public ProcessRegistrationAsyncTask(FragmentManager fm, String logErr, String emailErr) {
            this.fm = fm;
            this.logErr = logErr;
            this.emailErr = emailErr;
        }

        @Override
        protected void onPostExecute(Boolean succeed) {
            if (!succeed) {
                Toast.makeText(App.get(),
                        message != null ? message : "Ошибка при авторизации",
                        Toast.LENGTH_LONG).show();
               /* fm.beginTransaction()
                        .replace(R.id.rlLoginContainer, new RegisFragment())
                        .commitAllowingStateLoss();*/
            } else {
                Toast.makeText(App.get(), "Регистрация прошла успешно", Toast.LENGTH_SHORT).show();
                fm.beginTransaction()
                        .replace(R.id.login_fragment_container, new LoginFragment())
                        .commitAllowingStateLoss();
                authApp();
            }
        }

        @Override
        protected void onPreExecute() {
            message = null;
            Fragment fragment = new LogoFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.SHOULD_LOOP_KEY, true);
            /*fragment.setArguments(bundle);
            fm.beginTransaction()
                    .replace(R.id.rlLoginContainer, fragment)
                    .commitAllowingStateLoss();*/
        }

        @Override
        protected Boolean doInBackground(String... params) {
            final FormEncodingBuilder builder = new FormEncodingBuilder()
                    .add("username", params[0])
                    .add("password", params[1])
                    .add("email", params[2])
                    .add("profession", params[3]);
            userName = params[0];
            userPass = params[1];

            final Request request= new Request.Builder()
                    .url(ApiHelper.BASE_URL + "/user/create/")
                    .post(builder.build())
                    .build();

            try {
                Response response = App.getHttpClient().newCall(request).execute();
                String rs = response.body().string();
                if (response.isSuccessful()) {
                    UserToken userToken = new Gson().fromJson(rs, UserToken.class);

                    //todo save in sp
                    return true;
                } else {
                    if (rs.contains("username") && rs.contains("unique")) {
                        //message = "Логи должен быть уникальным";
                        message = logErr;
                    } else if (rs.contains("email") && rs.contains("unique")) {
                        //message = "Email должен быть уникальным";
                        message = emailErr;
                    }
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

    private void authApp() {
        new LoginAsyncTask(fm).execute(userName, userPass);
    }
};