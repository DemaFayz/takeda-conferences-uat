package com.example.demafayz.takedaconferencesuat.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.dataExamples.Drug;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.List;

/**
 * Created by demafayz on 03.09.15.
 */
public class DrugsAdapter extends ArrayAdapter<Drug> {

    private class ViewHolder{
        ImageView civDrugIcon;
        TextView tvDrugName;
        TextView tvDrugDesc;
    }

    private static final String TAG = "MyTag DrugsAdapter";
    private List<Drug> list;
    private LayoutInflater inflater;

    private ViewHolder vh = new ViewHolder();

    public DrugsAdapter(Context context, int resourse, List<Drug> list) {
        super(context, resourse, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.item_drug, parent, false);
        }

        vh.civDrugIcon = (ImageView) itemView.findViewById(R.id.civDrugIcon);
        vh.tvDrugName = (TextView) itemView.findViewById(R.id.tvDrugName);
        vh.tvDrugDesc = (TextView) itemView.findViewById(R.id.tvDrugDesc);

        UrlImageViewHelper.setUrlDrawable(vh.civDrugIcon, list.get(position).getImage(), R.drawable.alpha_pix, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String url, boolean loadedFromCache) {
                if (!loadedFromCache) {
                    ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
                    scale.setDuration(300);
                    scale.setInterpolator(new OvershootInterpolator());
                    imageView.startAnimation(scale);
                }
            }
        });
        vh.tvDrugName.setText(list.get(position).getName());
        vh.tvDrugDesc.setText(list.get(position).getShortDescription());
        return itemView;
    }
}