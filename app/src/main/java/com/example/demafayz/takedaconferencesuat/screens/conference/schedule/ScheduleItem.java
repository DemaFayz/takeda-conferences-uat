package com.example.demafayz.takedaconferencesuat.screens.conference.schedule;

import com.example.demafayz.takedaconferencesuat.api.ApiHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by demafayz on 27.08.15.
 */
public class ScheduleItem {
    long headerId;
    String dateHeader;
    String title;
    String lecturer;
    long dateStart;
    long dateEnd;
    boolean header;
    String room;
    int x_coord;
    int y_coord;
    String mapBackgroundUrl;


    public ScheduleItem(String title, String lecturer, long dateStart, long dateEnd, String room, long headerId,
                        boolean header, int x, int y, String mapUrl) {
        //this.dateHeader = room;

        this.dateHeader = ApiHelper.longDateToString(dateStart, "dd MMMM HH:mm") + " - " + ApiHelper.longDateToString(dateEnd, "HH:mm") + "\n" + room;

        //this.dateHeader = ApiHelper.longDateToString(dateStart, "d MMMM H:mm") + " - " + longDateToString(dateEnd, "H:mm" + "\n" + room);

        this.header = header;
        this.title = title;
        this.lecturer = lecturer;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.room = room;
        this.headerId = headerId;
        this.x_coord = x;
        this.y_coord = y;
        this.mapBackgroundUrl = mapUrl;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", ApiHelper.myDateFormatSymbols );
        System.out.println("Constructor 3: " + dateFormat.format(new Date(dateEnd)));
    }

    public String getMapBackgroundUrl() {
        return mapBackgroundUrl;
    }

    public int getX_coord() {
        return x_coord;
    }

    public int getY_coord() {
        return y_coord;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public long getHeaderId() {
        return headerId;
    }

    public void setHeaderId(long headerId) {
        this.headerId = headerId;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDateHeader() {
        return dateHeader;
    }

    public void setDateHeader(String dateHeader) {
        this.dateHeader = dateHeader;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    private String longDateToString(long date, String dateFormat) {

        if (dateFormat == null || dateFormat == "") {
            Date date1 = new Date();
            date1.setTime(date);

            SimpleDateFormat formatter = new SimpleDateFormat("d MMMM H:m");
            String formattedDate = formatter.format(date1);
            return formattedDate;
        } else {
            Date date1 = new Date();
            date1.setTime(date);

            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            String formattedDate = formatter.format(date1);
            return formattedDate;
        }

    }
}
