package com.example.demafayz.takedaconferencesuat.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by demafayz on 26.08.15.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "TAKEDA_DB";

    public class TABLE_CONFERENCE {
        public static final String TABLE_NAME = "CONFERENCE";
        public static final String PK = "PK";
        public static final String TITLE = "TITLE";
        public static final String START_DATE = "START_DATE";
        public static final String END_DATE = "END_DATE";
        public static final String CITY = "CITY";
        public static final String ADDRESS = "ADDRESS";
        public static final String DESC = "DESCRIPTION";
        public static final String LOGO = "LOGO";
        public static final String MAP = "MAP";
        public static final String HALLS = "HALLS";
        public static final String MAP_ENTITIES = "MAP_ENTITIES";
    }

    public class APP_TEXT {
        public static final String TABLE_NAME = "APP_TEXT";
        public static final String PK = "PK";
        public static final String TAKEDA = "TAKEDA";
        public static final String WARNING = "WARNING";
        public static final String AGREEMENT = "AGREEMENT";
    }

    public class TOKEN {
        public static final String TABLE_NAME = "TOKEN";
        public static final String EXPIRES_IN = "EXPIRES_IN";
        public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
        public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
        public static final String SCOPE = "SCOPE";
        public static final String TOKEN_TYPE = "TOKEN_TYPE";
    }

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_CONFERENCE.TABLE_NAME + " ("
                + TABLE_CONFERENCE.PK + " INTEGER, "
                + TABLE_CONFERENCE.TITLE + " TEXT, "
                + TABLE_CONFERENCE.START_DATE + " INTEGER, "
                + TABLE_CONFERENCE.END_DATE + " INTEGER, "
                + TABLE_CONFERENCE.CITY + " TEXT, "
                + TABLE_CONFERENCE.ADDRESS + " TEXT, "
                + TABLE_CONFERENCE.DESC + " TEXT, "
                + TABLE_CONFERENCE.LOGO + " TEXT, "
                + TABLE_CONFERENCE.MAP + " TEXT, "
                + TABLE_CONFERENCE.HALLS + " TEXT"
                + ");");

        db.execSQL("CREATE TABLE " + APP_TEXT.TABLE_NAME + " ("
                + APP_TEXT.PK + " INTEGER, "
                + APP_TEXT.TAKEDA + " TEXT, "
                + APP_TEXT.WARNING + " TEXT, "
                + APP_TEXT.AGREEMENT + " TEXT"
                + ");");

        db.execSQL("CREATE TABLE " + TOKEN.TABLE_NAME + " ("
                + TOKEN.EXPIRES_IN + " INTEGER, "
                + TOKEN.REFRESH_TOKEN + " TEXT, "
                + TOKEN.ACCESS_TOKEN + " TEXT, "
                + TOKEN.SCOPE + " TEXT, "
                + TOKEN.TOKEN_TYPE + " TEXT"
                + ");"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
