package com.example.demafayz.takedaconferencesuat.dataExamples;

/**
 * Created by demafayz on 03.09.15.
 */
public class  AppText {
    private long pk;
    private String takeda;
    private String warning;
    private String agreement;

    public AppText(long pk, String takeda, String warning, String agreement) {
        this.pk = pk;
        this.takeda = takeda;
        this.warning = warning;
        this.agreement = agreement;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getTakeda() {
        return takeda;
    }

    public void setTakeda(String takeda) {
        this.takeda = takeda;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement;
    }
}
