package com.example.demafayz.takedaconferencesuat.login;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.demafayz.takedaconferencesuat.dataExamples.Profession;

import java.util.List;

public class ProfessionsDropdownArrayAdapter extends ArrayAdapter<Profession> {
    private List<Profession> items;
    public ProfessionsDropdownArrayAdapter(Context context, int resource, List<Profession> objects) {
        super(context, resource, objects);
        items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        ((TextView) v.findViewById(android.R.id.text1)).setText(
                items.get(position).getTitle()
        );
        return v;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Profession getItem(int position) {
        return items.get(position);
    }
}
