package com.example.demafayz.takedaconferencesuat.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgreementFragment extends Fragment {


    public AgreementFragment() {
        // Required empty public constructor
    }

    private class ViewHolder {
        TextView tvAgreeText;
    }

    private ViewHolder vh = new ViewHolder();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_agreement, container, false);
        populateViewHolder(layout);
        MainActivity.setTitle("Соглашение");
        return layout;
    }

    private void populateViewHolder(View layout) {
        vh.tvAgreeText = (TextView) layout.findViewById(R.id.tvAgreeText);
        vh.tvAgreeText.setText(MainActivity.listAppText.get(MainActivity.listAppText.size() - 1).getAgreement());
    }


}
