package com.example.demafayz.takedaconferencesuat.fragments;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.dataExamples.Lecturer;
import com.example.demafayz.takedaconferencesuat.adapters.LecturersAdapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LecturerFragment extends Fragment {


    public LecturerFragment() {
        // Required empty public constructor
    }

    private class ViewHolder{
        ListView lvLecturers;
    }

    private ViewHolder vh = new ViewHolder();
    private List<Lecturer> list;
    private List<Lecturer> listSortResult;
    private LecturersAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_lecturer, container, false);
        populateViewHolder(layout);
        startRequest();
        //new GetLect().execute();
        return layout;
    }

    private void startRequest() {
        //new GetLect().execute();
        list = ApiHelper.getLecturersOnRootJson(MainActivity.rootJson);
        createNewAdapter(list);
    }

    public void createNewAdapter(List<Lecturer> list) {
        //dialog.cancel();
        populateListView();
        setAdapter(list);
    }

    private void setAdapter(List<Lecturer> list) {
        adapter = new LecturersAdapter(getActivity(), 0, list);
        vh.lvLecturers.setAdapter(adapter);
        //dialog.cancel();
    }

    private void populateListView() {

    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.setTitle("Лекторы");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lecturer, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listSortResult = AppSupport.sortLecturer(list, newText);
                if (listSortResult.size() > 0) {
                    createNewAdapter(listSortResult);
                    vh.lvLecturers.setVisibility(View.VISIBLE);
                } else {
                    vh.lvLecturers.setVisibility(View.GONE);
                }
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void populateViewHolder(View layout) {
        vh.lvLecturers = (ListView) layout.findViewById(R.id.lvLecturers);
    }

    /*private class GetLect extends AsyncTask<Void, Void, Void> {
        List<Lecturer> list = new ArrayList<>();
        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(LecturerFragment.this.getActivity(), null, "Идет запрос...", false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            list = ApiHelper.getLecturers();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LecturerFragment.this.list = list;
            dialog.cancel();
            createNewAdapter();
        }
    }*/
}