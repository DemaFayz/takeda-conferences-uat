package com.example.demafayz.takedaconferencesuat.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;

import com.example.demafayz.takedaconferencesuat.R;

/**
 * Created by demafayz on 15.09.15.
 */
public class ProgressDialog {

    private Context activity ;
    private Dialog dialog ;

    public ProgressDialog(Context activity) {
        this.activity = activity;
    }

    public void show() {
        dialog=new Dialog(activity, R.style.CustomDialogTheme) ;
        dialog.setContentView(R.layout.radio_progress_dialog);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //isCanceled = true;
            }
        });
        dialog.show();
    }

    public void dismiss () {
        dialog.dismiss();
    }

}
