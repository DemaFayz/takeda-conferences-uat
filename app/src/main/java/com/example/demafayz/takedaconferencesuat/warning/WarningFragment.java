package com.example.demafayz.takedaconferencesuat.warning;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WarningFragment extends Fragment {


    public WarningFragment() {
        // Required empty public constructor
    }

    private class ViewHolder {
        TextView tvWarningText;
    }

    private ViewHolder vh = new ViewHolder();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_warning, container, false);
        populateViewHolder(layout);
        MainActivity.setTitle("Предупреждение");
        return layout;
    }

    private void populateViewHolder(View layout) {
        vh.tvWarningText = (TextView) layout.findViewById(R.id.tvWarningText);
        vh.tvWarningText.setText(MainActivity.listAppText.get(MainActivity.listAppText.size() - 1).getWarning());
    }


}
