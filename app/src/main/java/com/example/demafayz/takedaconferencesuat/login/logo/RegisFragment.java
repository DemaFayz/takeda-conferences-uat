package com.example.demafayz.takedaconferencesuat.login.logo;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.login.NothingSelectedSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisFragment extends Fragment implements View.OnClickListener {


    public RegisFragment() {
        // Required empty public constructor
    }

    private ViewHolder vh = new ViewHolder();
    private FragmentActivity activity;
    private FragmentManager fm;

    private class ViewHolder {
        public EditText etEmail;
        public EditText etPass;
        public EditText etName;
        public EditText etSurname;
        public EditText etPhone;
        public Spinner spCity;
        public Spinner spSpecialty;
        public CheckBox cbAgreement;
        public TextView tvAgreement;
        public CheckBox cbEmployee;
        public TextView tvEmployee;
        public Button btnSingUp;
        public Button btnBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_sign_up, container, false);
        populateDatas();
        populateViewHolder(layout);
        setAdapters();
        return layout;
    }

    private void setAdapters() {

        //TODO specialty и citys - это тестовые данные

        List<String> specialty = new ArrayList<>();
        specialty.add("Хирург");
        specialty.add("Терапевт");
        specialty.add("Педиатор");

        List<String> citys = new ArrayList<>();
        citys.add("Казань");
        citys.add("Актаныш");
        citys.add("Кукмары");
        citys.add("Челны");

        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_specialty_selected_item, citys);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vh.spCity.setPrompt(activity.getResources().getString(R.string.sign_up_fragment__sp_city_hint));
        vh.spCity.setAdapter(new NothingSelectedSpinnerAdapter(cityAdapter, R.layout.spinner_city_unselected_item, activity));

        ArrayAdapter<String> specialtyAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_specialty_selected_item, specialty);
        specialtyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vh.spSpecialty.setPrompt(activity.getResources().getString(R.string.sign_up_fragment__sp_specialty_hint));
        vh.spSpecialty.setAdapter(new NothingSelectedSpinnerAdapter(specialtyAdapter, R.layout.spinner_specialty_unselected_item, activity));
    }

    private void populateViewHolder(View layout) {
        vh.etEmail = (EditText) layout.findViewById(R.id.et_email);
        vh.etPass = (EditText) layout.findViewById(R.id.et_pass);
        vh.etName = (EditText) layout.findViewById(R.id.et_name);
        vh.etSurname = (EditText) layout.findViewById(R.id.et_surname);
        vh.etPhone = (EditText) layout.findViewById(R.id.et_number);
        vh.spCity = (Spinner) layout.findViewById(R.id.sp_city);
        vh.spSpecialty = (Spinner) layout.findViewById(R.id.sp_specialty);
        vh.cbAgreement = (CheckBox) layout.findViewById(R.id.cb_agreement);
        vh.tvAgreement = (TextView) layout.findViewById(R.id.tv_agreement);
        vh.tvAgreement.setOnClickListener(this);
        vh.cbEmployee = (CheckBox) layout.findViewById(R.id.cb_employee);
        vh.tvEmployee = (TextView) layout.findViewById(R.id.tv_employee);
        vh.tvEmployee.setOnClickListener(this);
        vh.btnSingUp = (Button) layout.findViewById(R.id.btn_sign_up);
        vh.btnSingUp.setOnClickListener(this);
        vh.btnBack = (Button) layout.findViewById(R.id.btn_back);
        vh.btnBack.setOnClickListener(this);
    }

    private void populateDatas() {
        this.activity = getActivity();
        this.fm = activity.getSupportFragmentManager();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_sign_up:
                startApp();
                break;
            case R.id.btn_back:
                activity.onBackPressed();
                break;
        }
    }

    private void startApp() {
        Intent startAppIntent = new Intent(activity, MainActivity.class);
        startActivity(startAppIntent);
        activity.finish();
    }

    public static RegisFragment newInstance() {

        Bundle args = new Bundle();

        RegisFragment fragment = new RegisFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
