package com.example.demafayz.takedaconferencesuat.login.logo;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.util.Constants;


public class LogoFragment extends Fragment {

    private class ViewHolder {
        ImageView ivStartLogo;
    }

    private ViewHolder vh = new ViewHolder();
    private AlphaAnimation animationIn;
    private AlphaAnimation animationOut;
    private Activity mActivity;

    public LogoFragment() {

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity != null) mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_logo, container, false);
        populateViewHolder(layout);
        startAnimation();
        if (!shouldAnimationLoop()) {
            deadLogin();
        }
        return layout;
    }

    private void deadLogin() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mActivity.finish();
            }
        }, 10 * 1000);
    }

    private void startAnimation() {
        animationIn = new AlphaAnimation(0.0F, 1.0F);
        animationIn.setDuration(300);
        animationIn.setStartOffset(350);

        animationOut = new AlphaAnimation(1.0F, 0.0F);
        animationOut.setDuration(300);
        animationOut.setStartOffset(350);

        animationIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vh.ivStartLogo.startAnimation(animationOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vh.ivStartLogo.startAnimation(animationIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vh.ivStartLogo.startAnimation(animationIn);
    }

    private boolean shouldAnimationLoop() {
        return getArguments() != null && getArguments().getBoolean(Constants.SHOULD_LOOP_KEY);
    }

    private void populateViewHolder(View layout) {
        vh.ivStartLogo = (ImageView) layout.findViewById(R.id.ivStartLogo);
    }


}
