package com.example.demafayz.takedaconferencesuat.fragments;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.activitys.MainActivity;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.dataExamples.Drug;
import com.example.demafayz.takedaconferencesuat.adapters.DrugsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicineFragment extends Fragment {


    public MedicineFragment() {
        // Required empty public constructor
    }

    private class ViewHolder {
        ListView lvDrugs;
    }

    private ViewHolder vh = new ViewHolder();
    private List<Drug> list = new ArrayList<>();
    private ProgressDialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_medicine, container, false);
        populateViewHolder(layout);
        MainActivity.setTitle("Препараты");
        //new GetDrugs().execute();
        list = ApiHelper.getDrugOnRootJson(MainActivity.rootJson);
        createNewAdapter(list);
        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_drugs, menu);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Drug> listDrugResult = AppSupport.sortDrugsList(list, newText);
                if (listDrugResult.size() > 0) {
                    createNewAdapter(listDrugResult);
                    vh.lvDrugs.setVisibility(View.VISIBLE);
                } else {
                    vh.lvDrugs.setVisibility(View.GONE);
                }

                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }

    private void createNewAdapter(List<Drug> list) {
        DrugsAdapter adapter = new DrugsAdapter(MedicineFragment.this.getActivity(), 0, list);
        vh.lvDrugs.setAdapter(adapter);
    }

    private void populateViewHolder(View layout) {
        vh.lvDrugs = (ListView) layout.findViewById(R.id.lvDrugs);
    }

    /*private class GetDrugs extends AsyncTask<Void, Void, Void> {
        private List<Drug> list;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(MedicineFragment.this.getActivity(), null, "Идет запрос", false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            list = ApiHelper.get
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            MedicineFragment.this.list = list;
            createNewAdapter();
            dialog.dismiss();
        }
    }*/
}
