package com.example.demafayz.takedaconferencesuat.screens.conference.schedule;

import android.app.SearchManager;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.demafayz.takedaconferencesuat.AppSupport;
import com.example.demafayz.takedaconferencesuat.R;
import com.example.demafayz.takedaconferencesuat.api.ApiHelper;
import com.example.demafayz.takedaconferencesuat.screens.conference.ConferenceActivity;
import com.example.demafayz.takedaconferencesuat.screens.conference.schedule.navigation.ScheduleNavigationAdapter;
import com.example.demafayz.takedaconferencesuat.screens.conference.schedule.navigation.ScheduleNavigationDrawer;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class ScheduleActivity extends AppCompatActivity {

    private class ViewHolder {
        StickyListHeadersListView lvScheduleStickyList;
        ListView scheduleSpis;
    }

    private ViewHolder vh = new ViewHolder();
    private ScheduleAdapter mAdapter;
    private List<ScheduleItem> list = new ArrayList<>();
    private List<ScheduleItem> listSort = new ArrayList<>();
    private List<ScheduleItem> listSortResult;
    private List<String> navigationList = new ArrayList<>();
    private ScheduleNavigationAdapter scheduleNavigationAdapter;
    private Toolbar toolbar;
    private static final String TAG = "MyTag ScheduleActivity";
    private ScheduleNavigationDrawer drawerFragment;
    private String halls;
    public static final String SIMPLE_DATE_FORMAT_NAVIGATION = "dd MMMM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        populateViewHolder();
        populateActionBar("Расписание конгресса");
        populateList();
        createViewAdapter(list);
        onClickRegister();
    }

    private void onClickRegister() {
        final ListView drawerList = drawerFragment.getListView();
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(ScheduleActivity.this, String.valueOf("Click:" + position), Toast.LENGTH_LONG).show();

                drawerFragment.closeDrawer();
                String date = navigationList.get(position);
                if (position == navigationList.size() - 1) {
                    finish();
                    return;
                }
                if (position == 0) {
                    createViewAdapter(list);
                    return;
                }
                listSort.clear();
                for (int i = 0; i < list.size(); i++) {
                    if (date.equals(ApiHelper.longDateToString(list.get(i).getDateStart(), SIMPLE_DATE_FORMAT_NAVIGATION))) {
                        listSort.add(list.get(i));
                    }
                }
                createViewAdapter(listSort);
            }
        });
    }

    private void populateActionBar(String title) {
        toolbar = (Toolbar) findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void populateNavigationDrawer(List<String> list) {
        list.add("Назад");
        drawerFragment = (ScheduleNavigationDrawer) getSupportFragmentManager().findFragmentById(R.id.fScheduleNavDrawer);
        drawerFragment.setUp(R.id.fScheduleNavDrawer, (DrawerLayout) findViewById(R.id.drawer_schedule_layout), toolbar);
        drawerFragment.createNewAdapter(list);
    }

    private String parseDate(String strDate) throws ParseException {
        //SimpleDateFormat formatter=new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        SimpleDateFormat formatter=new SimpleDateFormat("d MMMM yyyy H:m", Locale.ENGLISH);
        Date date = formatter.parse(strDate);
        String formattedDate = formatter.format(date);
        Date date1 = formatter.parse(formattedDate);

        formatter = new SimpleDateFormat("d MMMM H:m");
        formattedDate = formatter.format(date1);
        return formattedDate;
    }

    private long stringToDateLong(String strDate) {
        //SimpleDateFormat formatter=new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Log.d(TAG, "SimpleDate: " + strDate);
        Date date = null;
        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long milliseconds = date.getTime();
        return milliseconds;
    }

    private static void bubbleSort(List<ScheduleItem> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (list.get(j).getDateStart() > list.get(j + 1).getDateStart()) {
                    ScheduleItem item = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, item);
                }
            }
        }
    }

    private void populateList() {

        JSONArray request = null;
        try {
            request = new JSONArray(ConferenceActivity.conferenceItem.getHalls());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        list = ApiHelper.getConferenceHalls(request, ConferenceActivity.conferenceItem.getMap());
        LinkedHashSet hashSet = new LinkedHashSet();
        bubbleSort(list);
        for (int i = 0; i < list.size(); i++) {
            hashSet.add(ApiHelper.longDateToString(list.get(i).getDateStart(), SIMPLE_DATE_FORMAT_NAVIGATION));
        }


        navigationList.clear();
        navigationList.add("Отобразить все");
        for (Iterator i = hashSet.iterator(); i.hasNext();) {
            navigationList.add((String) i.next());
        }
        populateNavigationDrawer(navigationList);
    }



    private void createViewAdapter(List<ScheduleItem> list) {
        if (scheduleNavigationAdapter != null) scheduleNavigationAdapter.notifyDataSetChanged();
        mAdapter = new ScheduleAdapter(this, list);
        vh.lvScheduleStickyList.setAdapter(mAdapter);
    }



    private void populateViewHolder() {
        vh.lvScheduleStickyList = (StickyListHeadersListView) findViewById(R.id.lvScheduleList);
        //vh.scheduleSpis = (ListView) findViewById(R.id.lvScheduleSpis);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.menu_schedule, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit Called: " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange Called: " + newText);
                if (newText.length() > 0) {
                    listSortResult = AppSupport.sortScheduleList(list, newText);
                    if (listSortResult.size() < 1) {
                        vh.lvScheduleStickyList.setVisibility(View.GONE);
                    } else {
                        vh.lvScheduleStickyList.setVisibility(View.VISIBLE);
                        createViewAdapter(listSortResult);
                    }
                } else {
                    vh.lvScheduleStickyList.setVisibility(View.VISIBLE);
                    createViewAdapter(list);
                }

                return true;
            }
        });
        return true;
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule, menu);

        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == android.R.id.home) {
            //NavUtils.navigateUpFromSameTask(this);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}