package com.example.demafayz.takedaconferencesuat.dataExamples;

import com.example.demafayz.takedaconferencesuat.App;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class MapEntityPoint {

    private final int x;
    private final int y;
    private final String description;

    public MapEntityPoint(int x, int y, String description) {
        this.x = x;
        this.y = y;
        this.description = description;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getDescription() {
        return description;
    }

    public static List<MapEntityPoint> fromJsonList(String jsonList) {
        Type listPoints = new TypeToken<List<MapEntityPoint>>() {}.getType();
        return App.gson().fromJson(jsonList, listPoints);
    }
}
